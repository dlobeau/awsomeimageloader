//
//  PinListJsonParser.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation


protocol JsonParser
{
     func parse<T:Decodable>(WithStream Stream: Data) -> (T?)
}


struct PinListJsonParserGeneric:JsonParser
{
    func parse<T:Decodable>(WithStream Stream: Data) -> (T?)
    {
        var ReturnValue:T?
        
        do
        {
            ReturnValue = try JSONDecoder().decode(T.self, from: Stream)
         }
        catch let parsingError
        {
            print("Error", parsingError)
        }
        return ReturnValue
    }
    
    
   
    
    
}

