//
//  LocalFile.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 05/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import AwsomeDataFetcher

protocol LocalFile:DataFetcher
{
    var completeFilePathAndName:String?{get}
    
    func cleanFileFromUserEnvironment() throws ->()
    func isFileExist()->(Bool)
}



class LocalFileImp:LocalFile,DataFetcherDelegate
{
    var fetchedData: Any?
     var identification:String
     {
        return self.completeFilePathAndName!
    }
    func didFetchData(WithFetchedData DataFectched: Any?)
    {
        self.fetchedData = DataFectched
    }
    
    var delegate: DataFetcherDelegate?
    
    
    private var bundle:Bundle
    private var fileContent:String?
    
    init( WithBundle FileBundle:Bundle)
    {
        self.bundle = FileBundle
    }
    
    var completeFilePathAndName:String?
    
  
     func fetch(WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate)->()
    {
        self.completeFilePathAndName = self.copyRessource(WithURL: URL)
        do
        {
            try  fileContent = String.init(contentsOfFile: self.completeFilePathAndName!)
        }
        catch
        {
            Delegate.didFetchData(WithFetchedData: "")
        }
        Delegate.didFetchData(WithFetchedData: fileContent!)
    }
    
    func fetchDirect(WithURL URL:String)->(Any?)
    {
        var ReturnValue:Any?
        
        self.completeFilePathAndName = self.copyRessource(WithURL: URL)
        do
        {
            try  ReturnValue = String.init(contentsOfFile: self.completeFilePathAndName!)
        }
        catch
        {
            self.delegate!.didFetchData(WithFetchedData: "")
        }
        return ReturnValue
    }
    
    func cleanFileFromUserEnvironment() throws ->()
    {
        let fileManager = FileManager.default
        let FileName = self.completeFilePathAndName;
        try fileManager.removeItem(atPath: FileName!)
     }

    func isFileExist()->(Bool)
    {
        var bReturnValue:Bool = true;
        
        let fileManager = FileManager.default
        
        if (!fileManager.fileExists(atPath:self.completeFilePathAndName!))
        {
            bReturnValue = false;
        }
        
        return bReturnValue;
    }

    func copyRessource(WithURL URL:String)->(String?)
    {
        let FolderPath = LocalFileImp.environmentRoot
        var ReturnValue:String? = nil
        do
        {
            try ReturnValue = self.copyRessourceFile(WithURL: URL, WithFolderDestination: FolderPath())
        }
        catch
        {
            
        }
        return ReturnValue
    }

   static func  environmentRoot()->(String)
      {
          var ReturnValue = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last
          
          ReturnValue = "\(ReturnValue!)/userApplication";
          
          return ReturnValue!;
      }

    func copyRessourceFile(WithURL URL:String ,WithFolderDestination FolderName:String) throws -> (String?)
    {
        let ReturnValue = "\(FolderName)/\(URL)"
               
        try self.createDirectory(WithFolderPath:FolderName)
        
        let fileManager = FileManager.default
        if (!fileManager.fileExists(atPath: ReturnValue))
        {
            let NameAndExtension = self.fileNameAndExtension(WithFileURL: URL)
            
            if let sourcePath = self.bundle.path(forResource: NameAndExtension.0, ofType: NameAndExtension.1)
            {
                let fileManager = FileManager.default
                
                let NewDestination  = "\(FolderName)/\(NameAndExtension.0)\(NameAndExtension.1)"
                
                try fileManager.copyItem(atPath: sourcePath, toPath: NewDestination)
            }
        }
        
        return ReturnValue;
    }

    func fileNameAndExtension(WithFileURL FileURL:String)->(String,String)
    {
        let DotIndex = FileURL.firstIndex(of: ".") ?? FileURL.endIndex
        let Extention = FileURL.suffix(from: DotIndex)
        let Name = FileURL.prefix(upTo: DotIndex)
               
        return (String(Name),String(Extention))
    }
    

    func createDirectory(  WithFolderPath  FolderPath:String)throws ->()
    {
        let fileManager =  FileManager.default
        var isDirectory:ObjCBool = false
       
        let doesFileExist = fileManager.fileExists(atPath: FolderPath, isDirectory: &isDirectory)
        if (!doesFileExist)
        {
            try fileManager.createDirectory(atPath: FolderPath, withIntermediateDirectories: true, attributes: nil)
        }
    }


    


    

  


}
