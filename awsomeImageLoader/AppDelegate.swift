//
//  AppDelegate.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import UIKit
import cleanProjectFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,KAASynchronousDataBindingDelegate
{
 
    var window: UIWindow?
    var coordinator:KAApplication?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        KAApplicationSerializableObjectImp.createInstance(withFileName: "PinBoardApplicationRemoteData",withDIDelegate: ContenerDI.init())
                   
        if let app = KAApplicationSerializableObjectImp.instance()
        {
            self.coordinator = app
            self.coordinator!.setAsynchronousDataBindingDelegate(self)
            self.coordinator!.start();
         }
        return true
    }

    //KAASynchronousDataBindingDelegate protocol
   func didASynchronousDataBindingFinished(withSender Sender: Any)
   {
        let environment = ProcessInfo.processInfo.environment
        if let mode = environment["mode"],
            mode == "PRODUCTION"
        {
            let MainControler:UIViewController = self.coordinator!.window()?.createView(withOwner: nil) as! UIViewController
                
            self.window = UIWindow.init(frame: UIScreen.main.bounds)
            self.window?.rootViewController = MainControler
            self.window?.makeKeyAndVisible()
        }
       
       
        
    }

   


}

