//
//  ApplicationManager.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import AwsomeDataFetcher

protocol PinBoardApplicationManager:KAApplication,KASeriazableObject
{
    var dataBase:PinsDataBase?{get}
    var imageFetcher:DataFetcher?{get}
}

class PinBoardApplicationManagerWithLocalSource :KAApplicationSerializableObjectImp,
    PinBoardApplicationManager
{
    var imageFetcher: DataFetcher?
    
    private var dataBaseVar:PinsDataBase?
   
    static override func getTypeTag() -> String!
    {
        return "mainApplication"
    }
    
    var dataBase:PinsDataBase?
    
    override func dataBinding() -> Bool
    {
        let Extract = super.datBaseContener()?.request(withID: "jsonDataBase")
        self.dataBase = Extract  as? PinsDataBase
        self.dataBase!.parser = PinListJsonParserGeneric()
        self.dataBase!.fetcher = LocalFileImp.init(WithBundle: Bundle.main)
       self.dataBase!.fetchData()
       
        return true
    }
    
    override func start()
    {
        if(!self.dataBinding())
        {
            print("Error during dataBinding in application")
        }
        self.imageFetcher = LocalFileImp.init(WithBundle: Bundle.main)
    }
}




