//
//  SwiftInjDependencyInjection.swift
//  cleanTaskList
//
//  Created by Didier Lobeau on 09/01/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import Swinject
import cleanProjectFramework


class ContenerDI: NSObject, KAInjectionDependencyDelegate
{
     let container = Container()
    
    func injectedObject(withParameters Parameters: [AnyHashable : Any]) -> Any
    {
       
        var ReturnValue:KASeriazableObject?
               
               
               if let Name = Parameters["group_id"] as? String
               {
                   switch Name
                   {
                       case PinBoardApplicationManagerWithLocalSource.getTypeTag():
                           ReturnValue = container.resolve(PinBoardApplicationManager.self, name: PinBoardApplicationManagerWithLocalSource.getTypeTag())
                    
                    case PinBoardApplicationManagerWithRemoteSource.getTypeTag():
                    ReturnValue = container.resolve(PinBoardApplicationRemoteManager.self, name: PinBoardApplicationManagerWithRemoteSource.getTypeTag())
                            
                       case PinsDataBaseSimpleComposite.getTypeTag():
                           ReturnValue = container.resolve(PinsDataBase.self, name: PinsDataBaseSimpleComposite.getTypeTag())
                       
                    
                    case PinBoardWindowPresenter.getTypeTag():
                                             ReturnValue = container.resolve(PinBoardWindow.self, name: PinBoardWindowPresenter.getTypeTag())
                                       
                    
                    case PinBoardCellsCollectionPresenter.getTypeTag():
                            ReturnValue = container.resolve(PinBoardCellsCollection.self, name: PinBoardCellsCollectionPresenter.getTypeTag())
                                       
                    
                    case PinCellPresenter.getTypeTag():
                            ReturnValue = container.resolve(PinCell.self, name: PinCellPresenter.getTypeTag())
                    
                    case ImagePresenter.getTypeTag():
                            ReturnValue = container.resolve(Image.self, name: ImagePresenter.getTypeTag())
                    
                    case QueryBusinessUseCaseAllPinsFromJson.getTypeTag():
                    ReturnValue = container.resolve(QueryBusinessUseCaseAllPins.self, name: QueryBusinessUseCaseAllPinsFromJson.getTypeTag())
                    
                    case PinBoardCollectionSectionPresenter.getTypeTag():
                    ReturnValue = container.resolve(PinBoardCollectionSection.self, name: PinBoardCollectionSectionPresenter.getTypeTag())
                                       
                       default:
                           ReturnValue = KAApplicationSerializableObjectImp.injectionDependencyDelegateDefaultItems()?.injectedObject(withParameters: Parameters) as? KASeriazableObject
                   }
                   ReturnValue?.setObjectFamilyName(Parameters["group_id"] as! String)
                   ReturnValue?.setID(Parameters["type_id"] as! String)
                   ReturnValue?.setLabel(Parameters["label"] as! String)
                   ReturnValue?.setLabelIdentifier(Parameters["identifier"] as! String)
                   
               }
               return ReturnValue!
    }
    
   
    
    override init()
    {
        super.init()
        setupContainers()
    }
    
    
    func setupContainers()->()
    {
        
        let Parameter:[String:String] = ["group_id":"","identifier":"","label":"","type_id":""]
           
        container.register(PinBoardApplicationRemoteManager.self, name: PinBoardApplicationManagerWithRemoteSource.getTypeTag(),
                              factory: { _ in PinBoardApplicationManagerWithRemoteSource(dictionary: Parameter) })
        
        container.register(PinBoardApplicationManager.self, name: PinBoardApplicationManagerWithLocalSource.getTypeTag(),
        factory: { _ in PinBoardApplicationManagerWithLocalSource(dictionary: Parameter) })
            
           container.register(PinsDataBase.self, name: PinsDataBaseSimpleComposite.getTypeTag(),
                              factory: { _ in PinsDataBaseSimpleComposite(dictionary: Parameter) })
        
        container.register(PinBoardWindow.self, name: PinBoardWindowPresenter.getTypeTag(),
        factory: { _ in PinBoardWindowPresenter(dictionary: Parameter) })
        
        container.register(PinBoardCellsCollection.self, name: PinBoardCellsCollectionPresenter.getTypeTag(),
        factory: { _ in PinBoardCellsCollectionPresenter(dictionary: Parameter) })
        
        container.register(PinCell.self, name: PinCellPresenter.getTypeTag(),
        factory: { _ in PinCellPresenter(dictionary: Parameter) })
        
        container.register(Image.self, name: ImagePresenter.getTypeTag(),
               factory: { _ in ImagePresenter(dictionary: Parameter) })
        
        container.register(QueryBusinessUseCaseAllPins.self, name: QueryBusinessUseCaseAllPinsFromJson.getTypeTag(),
        factory: { _ in QueryBusinessUseCaseAllPinsFromJson(dictionary: Parameter) })
        
        container.register(PinBoardCollectionSection.self, name: PinBoardCollectionSectionPresenter.getTypeTag(),
               factory: { _ in PinBoardCollectionSectionPresenter(dictionary: Parameter) })
              
        
        
              
        
    }
    
    
    
}
