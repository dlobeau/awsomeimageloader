//
//  ApplicationManagerWithRemoteData.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 11/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import AwsomeDataFetcher


protocol PinBoardApplicationRemoteManager:PinBoardApplicationManager,DataBaseRefreshedDelegate
{
}

class PinBoardApplicationManagerWithRemoteSource :KAApplicationSerializableObjectImp,
PinBoardApplicationRemoteManager
    
{
    func dataBaseDataReady()
    {
        self.asynchronousDataBindingDelegate()?.didASynchronousDataBindingFinished(withSender: self)
        super.dataBinding()
    }
    
     private var dataBaseVar:PinsDataBase?
       var imageFetcher:DataFetcher?
       
       static override func getTypeTag() -> String!
       {
           return "mainApplicationWithRemoteData"
       }
       
       var dataBase:PinsDataBase?
       
       override func dataBinding() -> Bool
       {
            return true
       }
       
       override func start()
       {
        
        
            let Extract = super.datBaseContener()?.request(withID: "jsonDataBase")
            self.dataBase = Extract  as? PinsDataBase
            self.dataBase!.parser = PinListJsonParserGeneric()
        
            let Fetcher = RemoteDataFetcherAlamo.init(WithCacheMaxSize: 1000000000)
            self.dataBase!.fetcher = Fetcher
            self.dataBase?.dataBaseReadyDelegate =  self
            self.asynchronousDataBinding()
            
            self.imageFetcher = Fetcher
       }
    
        override func asynchronousDataBinding() -> ()
        {
            self.dataBase!.fetchData()
        }
    
}
