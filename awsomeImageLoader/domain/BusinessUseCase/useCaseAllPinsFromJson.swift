//
//  useCaseAllPinsFromJson.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 07/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

import cleanProjectFramework


protocol QueryBusinessUseCaseAllPins:KAQueryBusinessUseCase,KASeriazableObject
{
}


class QueryBusinessUseCaseAllPinsFromJson:KASeriazableObjectTableImp,QueryBusinessUseCaseAllPins
{
    static override func getTypeTag() -> String!
    {
        return "useCaseAllPinsFromJson"
    }
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!)
    {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
           super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
    }
      
    
    func execute(withRepository Repository: KADomain!, withValue Value: KADomain!) -> KADomain!
    {
        let DataBaseLink = Repository as? KADomainLink
        
        return DataBaseLink
    }
    
    
}
