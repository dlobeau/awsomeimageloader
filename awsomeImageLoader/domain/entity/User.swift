//
//  User.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 05/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework



protocol User:Decodable,KADomain
{
    var id:String {get}
    var userName:String {get}
    var name:String {get}
    
    
}

class UserGeneric:KAGenericDomain,User
{
    enum CodingKeys:String,CodingKey
    {
        case id
        case name
        case userName = "username"
        case profileImage = "profile_image"
        case userLinks 
    }
    
    var id: String
    var name: String
    var userName: String
   
    
    required init(from decoder:Decoder) throws
    {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decode(String.self, forKey: .id)
            name = try values.decode(String.self, forKey: .name)
            userName = try values.decode(String.self, forKey: .userName)
            super.init(label: "", withLabelIdentifier: "", withObjectFamilyName: "user", withID: id)
    }
}
