//
//  PinBoard.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 05/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation

protocol PinBoard
{
    var pins:[Pin]?{get}
    
    
    
}

class PinBoardGeneric:PinBoard
{
    var pins: [Pin]?
    
}
