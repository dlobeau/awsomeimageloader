//
//  pin.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 04/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework




protocol Pin:KADomain,Decodable
{
    var id:String {get}
    var date:String{get}
    var width:Int{get}
    var height:Int{get}
    var color:String{get}
    var likesNumber:Int{get}
    var islikeByUser:Bool {get}
    var user:User {get}
    var urls:PinUrl{get}
    
}


class PinGeneric:KAGenericDomain, Pin
{
   
    
    enum CodingKeys:String,CodingKey
    {
        case id
        case date = "created_at"
        case width
        case height
        case islikeByUser = "liked_by_user"
        case color = "color"
        case likesNumber = "likes"
        case user = "user"
        case urls
    }
    
    var id:String
    var date:String
    var width:Int
    var height:Int
    var color:String
    var likesNumber:Int
    var islikeByUser:Bool
    var user:User
    var urls:PinUrl
    
    required init(from decoder:Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        date = try values.decode(String.self, forKey: .date)
        width = try values.decode(Int.self, forKey: .width)
        height = try values.decode(Int.self, forKey: .height)
        
        color = try values.decode(String.self, forKey: .color)
        likesNumber = try values.decode(Int.self, forKey: .likesNumber)
        islikeByUser = try values.decode(Bool.self, forKey: .islikeByUser)
        user = try values.decode(UserGeneric.self, forKey: .user)
        urls = try values.decode(PinUrlGeneric.self, forKey: .urls)
        super.init(label: "", withLabelIdentifier: "", withObjectFamilyName: "pin", withID: id)
    }
    
    override func request(withID RequestID: String!) -> KADomain!
    {
        var ReturnValue:KADomain?
        
        switch RequestID {
        case "thumbnail":
            ReturnValue = KAGenericDomain.createGenericObject(withLabel: self.urls.thumb) as? KADomain
            ReturnValue?.setID("thumbnail")
      
        default:
            ReturnValue = super.request(withID: RequestID)
        }
        return ReturnValue!
    }

   
}


protocol image
{
    var path:url{get}
}

protocol url
{
    var path:String{get}
}

protocol currentUserCollection
{
    
}

protocol categorie
{
    var Id:String {get}
    var title:String {get}
    var photoCount:Int {get}
    var categoryLink:[String:url]{get}
}


