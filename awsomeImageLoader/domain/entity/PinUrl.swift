//
//  PinUrl.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol PinUrl:Decodable,KADomain
{
    var raw:String{get}
    var full:String{get}
    var regular:String{get}
    var small:String{get}
    var thumb:String{get}
}

class PinUrlGeneric:KAGenericDomain, PinUrl
{
    enum CodingKeys:String,CodingKey
    {
        case raw
        case full
        case regular
        case small
        case thumb
        
    }
    
    var raw: String
    var full: String
    var regular: String
    var small:String
    var thumb: String
    
    
     required init(from decoder:Decoder) throws
     {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        raw = try values.decode(String.self, forKey: .raw)
        full = try values.decode(String.self, forKey: .full)
        regular = try values.decode(String.self, forKey: .regular)
        small = try values.decode(String.self, forKey: .small)
        thumb = try values.decode(String.self, forKey: .thumb)
        super.init(label: "", withLabelIdentifier: "", withObjectFamilyName: "url", withID:"url")
     }
}
