//
//  pinsJsonDataBase.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework
import AwsomeDataFetcher


protocol DataBaseRefreshedDelegate:NSObject
{
    func dataBaseDataReady()->()
}


protocol PinsDataBase:KADomain,KASeriazableObject
{
    func pins() -> ([Pin]?)
   
    var dataBaseReadyDelegate:DataBaseRefreshedDelegate?{get set}
    
    func fetchData()->()
    
    var parser:JsonParser?{get set}
    var fetcher:DataFetcher?{get set}
}

class PinsDataBaseSimpleComposite: KAGenericDomain,PinsDataBase,DataFetcherDelegate
{
    
    
    var fetchedData: Any?
    var identification:String
    {
        return self.id() + self.labelIdentifier()
    }
    
    func didFetchData(WithFetchedData DataFectched: Any?)
    {
            let  DataObject = DataFectched as! Data
            var Values:[PinGeneric]?
            Values = parser!.parse(WithStream:DataObject)
            fetchedData = Values
            self.dataBaseReadyDelegate?.dataBaseDataReady()
        
    }
    
    weak var dataBaseReadyDelegate: DataBaseRefreshedDelegate?
    
    var parser: JsonParser?
    var fetcher: DataFetcher?
    
    static override func getTypeTag() -> String!
    {
        return "pinsJsonDataBase"
    }
    
    func pins() -> ([Pin]?)
    {
        return fetchedData as? [Pin]
    }
    
    func fetchData()->()
    {
        fetcher!.fetch(WithURL: self.label(),WithDelegate: self)
    }
   
    override func request(withID RequestID: String!) -> KADomain!
    {
       var ReturnValue:KADomain?
       
       switch RequestID {
       case "allPins":
           ReturnValue = self;
       default:
           ReturnValue = super.request(withID: RequestID)
       }
       return ReturnValue!
    }

    
    override func childs() -> [KADomain]!
    {
        return (self.pins()!)
    }
}

