//
//  pinboardCellImageView.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class pinboardCellImageView: TLUIImageViewFacade,KAASynchronousDataBindingDelegate
{
    var delegateImageLoad:ImageLoadedInCellDelegate?
   
    func didASynchronousDataBindingFinished(withSender Sender: Any)
    {
        let Presenter = self.interface() as? ImagePresenter
        self.image = Presenter?.image
        delegateImageLoad?.imageLoadedImCell(WithCell:self)
    }
    
    override func setContent()
    {
        let Presenter = self.interface() as? ImagePresenter
        Presenter?.setAsynchronousDataBindingDelegate(self)
        Presenter?.asynchronousDataBinding()
        self.image = Presenter?.image
    }
}
