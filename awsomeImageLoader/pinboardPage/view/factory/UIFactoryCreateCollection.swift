//
//  UIFactoryCreateCollection.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


class UIFactoryCreateCollection:KACreateUIWidgetCollection
{
    override func create(withOwner Owner: KAView!, withWidgetType WidgetType: String!, withWidgetIdentifier identifier: String!, withSender Sender: KAPresenter!) -> KAView!
    {
              
        let ReturnValue = super.create(withOwner: Owner, withWidgetType: WidgetType, withWidgetIdentifier: identifier, withSender: Sender)
        return ReturnValue;
    }
    
    override func getChildWithID(_ ID: String!, withWidgetParent WidgetParent: KAView!) -> KAView!
    {
        let ReturnValue = super.getChildWithID(ID, withWidgetParent: WidgetParent)
        return ReturnValue
    }
    
}
