//
//  UIFactoryCreateCollectionCell.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class UIFactoryCreateCollectionCell:KACreateUIWidgetGenericFactory
{
    
   
    
    override func create(withOwner Owner: KAView!, withWidgetType WidgetType: String!, withWidgetIdentifier identifier: String!, withSender Sender: KAPresenter!) -> KAView!
    {
              
        let Table = Owner.view() as? UICollectionView;
        
        let ReturnValue =  Table!.dequeueReusableCell(withReuseIdentifier: identifier, for: IndexPath.init(row: 0, section: 0)) as! KAView
        
        
         return ReturnValue;
    }
}
