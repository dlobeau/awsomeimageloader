//
//  pinboardPageView.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework


protocol ImageLoadedInCellDelegate
{
    func imageLoadedImCell(WithCell Cell:pinboardCellImageView)->()
}

class PinboardPageView:TLUIFacadeViewController,UICollectionViewDelegate,UICollectionViewDataSource,ImageLoadedInCellDelegate
{
    @IBOutlet weak var collectionView: PinBoardCollectionView!
    
    override func viewDidLoad()
    {
        collectionView.dataSource = self
        collectionView.delegate = self
        let Layout = collectionView.collectionViewLayout as? PinterestLayout
        Layout?.delegate = self
    }
     
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated);
        self.interface().dataBinding()
        self.setContent()
    }
    
    override func setContent()
    {
        self.collectionView.setContent()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        let CollectionPresenter = self.collectionView.interface() as? PinBoardCellsCollection
        return CollectionPresenter!.sections().count;
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        let CollectionPresenter = self.collectionView.interface() as? PinBoardCellsCollection
        return CollectionPresenter!.section(at: section).cells().count ;
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let CollectionPresenter = self.collectionView.interface() as? PinBoardCellsCollection
        let  CellPresenter = CollectionPresenter!.section(at: indexPath.section).cell(at: indexPath.row) ;
        
        let CellView = CellPresenter.createView(withOwner: self.collectionView) as! pinboardCellView
        CellView.delegateImageLoad = self
        CellView.setContent()
        return CellView
    }

    func imageLoadedImCell(WithCell Cell:pinboardCellImageView)->()
    {
        let Layout =  self.collectionView.collectionViewLayout as? PinterestLayout
        Layout?.refresh()
        Layout?.invalidateLayout()
    }
}

extension PinboardPageView: PinterestLayoutDelegate
{
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
    {
        let CollectionPresenter = self.collectionView.interface() as? PinBoardCellsCollection
        
        let  CellPresenter = CollectionPresenter!.section(at: indexPath.section).cell(at: indexPath.row) as? PinCell ;
        let Height =  CellPresenter?.thumbnailImage?.height
        
        return Height!
    }
    
}
