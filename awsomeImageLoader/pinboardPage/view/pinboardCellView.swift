//
//  pinboardCellView.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

class pinboardCellView:TLUICollectionViewCellFacade,ImageLoadedInCellDelegate
{
    @IBOutlet weak var imageContener: pinboardCellImageView!
    var delegateImageLoad:ImageLoadedInCellDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.imageContener.delegateImageLoad = self
    }
    
    
    func imageLoadedImCell(WithCell Cell: pinboardCellImageView)
    {
        self.delegateImageLoad?.imageLoadedImCell(WithCell: Cell)
    }
    
    override func setContent()
    {
        imageContener.setContent()
    }
    
}
