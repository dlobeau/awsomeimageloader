//
//  pinBoardWindow.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol PinBoardWindow:KAPresenter,KASeriazableObject
{
    var pinBoardCollection:PinBoardCellsCollection? { get }
  
}

class PinBoardWindowPresenter: KAGenericPresenter,PinBoardWindow
{
    var pinBoardCollection:PinBoardCellsCollection?
    {
        return self.getChildwithIdentifier("collection") as? PinBoardCellsCollection
    }
    
    static override func getTypeTag() -> String! {
           return "pinBoardWindow"
       }
    
    
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(PinBoardWindowPresenterConformToDomain.init())
        self.setViewFactoryDelegate(KACreateWindowFactory.init())
    }
    
}

class PinBoardWindowPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let PAge = Sender as! PinBoardWindow
        let DataBase = Domain as! PinsDataBase
        
         
        let Delegate = PAge.pinBoardCollection!.testdelegate?()
        
        Delegate?.expectSender(PAge.pinBoardCollection!, conformTo: DataBase)
    }
}
