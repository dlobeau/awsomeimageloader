//
//  PinPresenter.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit
import cleanProjectFramework

protocol PinCell:KACell,KASeriazableObject
{
    var thumbnailImage:Image?{get}
}

class PinCellPresenter:KACellPresenter,PinCell
{
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(PinCellPresenterConformToDomain.init())
        self.setViewFactoryDelegate(UIFactoryCreateCollectionCell.init() )
    }
    
    static override func getTypeTag() -> String! {
        return "pinBoardCollectionCell"
    }
    
    var thumbnailImage:Image?
    {
        return self.getChildwithIdentifier("image") as? Image
    }
    
}
class PinCellPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let CurrentCell = Sender as! PinCell
        let PinData = Domain as! Pin

        let Delegate = CurrentCell.thumbnailImage?.testdelegate?()
        Delegate?.expectSender!(CurrentCell.thumbnailImage!, conformToText:PinData.urls.thumb)
    }
    
}
