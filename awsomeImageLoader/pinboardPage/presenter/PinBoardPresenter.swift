//
//  PinBoardPresenter.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import cleanProjectFramework

protocol PinBoardCellsCollection:KATable,KASeriazableObject
{
    
}

class PinBoardCellsCollectionPresenter:KATablePresenter,PinBoardCellsCollection
{
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(PinBoardCellsCollectionPresenterConformToDomain.init())
        self.setViewFactoryDelegate(UIFactoryCreateCollection.init())
       
    }
    
    static override func getTypeTag() -> String! {
        return "pinBoardCollection"
    }
    
}


class PinBoardCellsCollectionPresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo Domain: KADomain)
    {
        let Table = Sender as! PinBoardCellsCollection
        let DataBase = Domain as! PinsDataBase
        
        let pinList =  DataBase.pins()
        let Cells = Table.section(at: 0).cells()
        
        assert(pinList?.count == Cells.count, "Cells number and pins number not equal, \(pinList!.count), and \( Cells.count )")
        
        var i = 0;
        for Cell in Cells
        {
            let currentPinCell = Cell as! PinCell
            let currentPin = pinList![i]
            i+=1
           
            let Delegate = currentPinCell.testdelegate?()
            Delegate?.expectSender(currentPinCell, conformTo: currentPin)
         }
    }
}
