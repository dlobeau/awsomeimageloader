//
//  PinThumbnailImage.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 07/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import UIKit
import cleanProjectFramework
import AwsomeDataFetcher



protocol Image:KAPresenter,KASeriazableObject
{
    var height:CGFloat?{get}
    
}

class ImagePresenter:KAGenericPresenter,Image,DataFetcherDelegate
{
    var fetchedData: Any?
    var height: CGFloat?
    var  image:UIImage?
    var identification:String
    {
        return self.labelIdentifier() + self.id()
    }
    override init!(dictionary Dictionary: [AnyHashable : Any]!) {
        super.init(dictionary: Dictionary)
    }
    
    override init!(label: String!, withLabelIdentifier ID: String!, withObjectFamilyName GroupId: String!, withID TypeId: String!)
    {
        super.init(label: label, withLabelIdentifier: ID, withObjectFamilyName: GroupId, withID: TypeId)
        self.setTestdelegate(ImagePresenterConformToDomain.init())
        self.height = 0.0
    }
    
    static override func getTypeTag() -> String! {
        return "thumbnail"
    }
    
    //DataFetcherDelegate
    func didFetchData(WithFetchedData DataFectched: Any?)
    {
        if let DataFetchedConverted = DataFectched as? Data
        {
            if let ImageFetch = UIImage(data: DataFetchedConverted)
            {
                self.image = ImageFetch
                self.height = ImageFetch.size.height
                super.dataBinding()
            }
        }
        self.asynchronousDataBindingDelegate()?.didASynchronousDataBindingFinished(withSender: self)
    }
   
    override func getImage() -> UIImage!
    {
        return self.image
    }
     
    override func dataBinding() -> Bool {
        return false
    }
    
    override func asynchronousDataBinding() -> ()
    {
        let Application = KAApplicationSerializableObjectImp.instance() as? PinBoardApplicationManager
        let Fetcher = Application!.imageFetcher
        let Path = self.getText()
        Fetcher?.fetch(WithURL: Path!, WithDelegate: self)
    }
    
    
}

class ImagePresenterConformToDomain:NSObject,KAPresenterConformToDomain
{
    func expectSender(_ Sender: KAPresenter, conformTo PathImageDomain: KADomain)
    {
        let CurrentImage = Sender as! Image
      
        
         CurrentImage.dataBinding()
     
        let UnderTest = CurrentImage.getText?()
        let Reference =  PathImageDomain.labelIdentifier()
        
        assert( UnderTest == Reference , "image not equal" )
    }
    
    func expectSender(_ Sender: KAPresenter, conformToText ReferenceText: String)
    {
        let CurrentImage = Sender as! Image
         
        CurrentImage.asynchronousDataBinding?()
            
        let UnderTest = CurrentImage.getText?()
         assert( UnderTest == ReferenceText , "image not equal" )
    }
    
}

