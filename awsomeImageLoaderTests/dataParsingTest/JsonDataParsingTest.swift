//
//  JsonDataParsingTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest

class JsonDataParsingTest: XCTestCase
{
    let path:String = "1WcGvnm5.json"
    var fetcher:LocalFileImp?
    var parser:JsonParser?
    var pinList:[PinGeneric]?
    
    override func setUp()
    {
        fetcher = LocalFileImp.init(WithBundle: Bundle.main)
        let Stream = fetcher!.fetchDirect(WithURL: path) as! String
        
        parser = PinListJsonParserGeneric()
        pinList = parser!.parse(WithStream: Data(Stream.utf8))
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_IDShouldBe_4kQA1aQK8_Y()
     {
         let UnderTest = pinList?[0].id
         let Reference = "4kQA1aQK8-Y"
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationDateShouldBe_2016_05_29T15_42_02_04_00()
     {
         let UnderTest = pinList?[0].date
         let Reference = "2016-05-29T15:42:02-04:00"
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationWidthShouldBe_2448()
     {
         let UnderTest = pinList?[0].width
         let Reference = 2448
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationHeightShouldBe_1836()
     {
         let UnderTest = pinList?[0].height
         let Reference = 1836
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationIsLikeByUserShouldBe_false()
     {
         let UnderTest = pinList?[0].islikeByUser
         let Reference = false
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationColorShouldBe_060607()
     {
         let UnderTest = pinList?[0].color
         let Reference = "#060607"
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_CreationLikesNumberShouldBe_12()
     {
         let UnderTest = pinList?[0].likesNumber
         let Reference = 12
         XCTAssertTrue(Reference == UnderTest!,"\(UnderTest!) instead of \(Reference)")
     }
     
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_UserShouldBeconformToSource()
     {
         let UnderTestID = pinList?[0].user.id
         let UnderTestName = pinList?[0].user.name
         let UnderTestUserName = pinList?[0].user.userName
         
         let ReferenceId = "OevW4fja2No"
         let ReferenceUserName = "nicholaskampouris"
         let ReferenceName = "Nicholas Kampouris"
         
         XCTAssertTrue(ReferenceId == UnderTestID!,"\(UnderTestID!) instead of \(ReferenceId)")
         XCTAssertTrue(ReferenceName == UnderTestName!,"\(UnderTestName!) instead of \(ReferenceName)")
         XCTAssertTrue(ReferenceUserName == UnderTestUserName!,"\(UnderTestUserName!) instead of \(ReferenceUserName)")
     }
      
     func testJsonDataParsing_whenParsingJsonStream_PinOnIdex0_UrlsShouldBeconformToSource()
     {
         let UnderTestRaw = pinList?[0].urls.raw
         let UnderTestFull = pinList?[0].urls.full
         let UnderTestRegular = pinList?[0].urls.regular
         let UnderTestSmall = pinList?[0].urls.small
         let UnderTestThumb = pinList?[0].urls.thumb
         
         let ReferenceRaw = "https://images.unsplash.com/photo-1464550883968-cec281c19761"
         let ReferenceFull = "https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=4b142941bfd18159e2e4d166abcd0705"
         let ReferenceRegular = "https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=1080&fit=max&s=1881cd689e10e5dca28839e68678f432"
         let ReferenceSmall = "https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&w=400&fit=max&s=d5682032c546a3520465f2965cde1cec"
          let ReferenceThumb = "photo-1464550883968-cec281c19761.jpg"
         
         XCTAssertTrue(ReferenceRaw == UnderTestRaw!,"\(UnderTestRaw!) instead of \(ReferenceRaw)")
         XCTAssertTrue(ReferenceFull == UnderTestFull!,"\(UnderTestFull!) instead of \(ReferenceFull)")
         XCTAssertTrue(ReferenceRegular == UnderTestRegular!,"\(UnderTestRegular!) instead of \(ReferenceRegular)")
         XCTAssertTrue(ReferenceSmall == UnderTestSmall!,"\(UnderTestSmall!) instead of \(ReferenceSmall)")
         XCTAssertTrue(ReferenceThumb == UnderTestThumb!,"\(UnderTestThumb!) instead of \(ReferenceThumb)")
     }
}
