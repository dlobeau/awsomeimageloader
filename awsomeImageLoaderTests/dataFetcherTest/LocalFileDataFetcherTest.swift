//
//  LocalFileDataFetcherTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest

class LocalFileDataFetcherTest: XCTestCase
{
    let path:String = "1WcGvnm5.json"
    var fetcher:LocalFileImp?
     
    override func setUp()
    {
        fetcher = LocalFileImp.init(WithBundle: Bundle.main)
    }

    override func tearDown()
    {
        
    }

    func testLocalFileDataFetcher_whenFetchingJsonStream_StringStreamSizeShouldBe_29815()
    {
        let Stream = fetcher!.fetchDirect(WithURL: path) as! String
         let Reference = 8103
        XCTAssertTrue(Stream.count == Reference,"\(Stream.count) instead of \(Reference)")
    }

}
