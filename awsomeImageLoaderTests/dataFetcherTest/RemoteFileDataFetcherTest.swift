//
//  RemoteFileDataFetcherTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import AwsomeDataFetcher

class RemoteFileDataFetcherTest: XCTestCase
{
    let path:String = "https://pastebin.com/raw/1WcGvnm5"
    var fetcher:DataFetcher?
    
   
    
    override func setUp()
    {
        fetcher = RemoteDataFetcherAlamo.init(WithCacheMaxSize: 1000000000)
    }

    override func tearDown()
    {
       
    }

    func testRemoteFileDataFetcher_whenFetchingJsonStream_StringStreamSizeShouldBe_29815()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestFetcherDelegate.init(WithExpectation: expectation)
        fetcher!.fetch(WithURL: path,WithDelegate: currentDelegate)
        
        waitForExpectations(timeout: 5, handler: nil)
        
        let Reference = 30400
        let Stream = currentDelegate.fetchedData as! Data
        XCTAssertTrue(Stream.count == Reference,"\(Stream.count) instead of \(Reference)")
    }

}
