//
//  ApplicationDataBaseRemoteTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework
class ApplicationDataBaseRemoteTest: XCTestCase
{

    var testEnvironment:TestEnvironment?
    var app:PinBoardApplicationRemoteManager?
    var dataBase:PinsDataBase?
    var pinList:[Pin]?
    
    class TestApplicationStarted:NSObject, KAASynchronousDataBindingDelegate
    {
        func didASynchronousDataBindingFinished(withSender Sender: Any)
        {
            self.fetchingExpectation!.fulfill()
            self.fetchingExpectation = nil
        }
        
        var fetchingExpectation:XCTestExpectation?
       
        init(WithExpectation Expectation:XCTestExpectation)
        {
            self.fetchingExpectation = Expectation
        }
        
    }
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "PinBoardApplicationRemoteData")
        app = testEnvironment!.application as? PinBoardApplicationRemoteManager
        
    }

    override func tearDown()
    {
    }

    func testApplicationDataBaseRemoteTest_whenFetchingRemoteDatabase_10PinSHouldRetrieved()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
        
        waitForExpectations(timeout: 10, handler: nil)
        
        dataBase = testEnvironment!.dataBase
        
        pinList = dataBase?.pins()
        let UnderTest = pinList!.count
        let Reference = 10
        XCTAssertTrue(Reference == UnderTest,"\(UnderTest) instead of \(Reference)")
    }

}
