//
//  ApplicationDataBaseTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class ApplicationDataBaseTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:KAApplication?
    var pinList:[Pin]?
    var dataBase:PinsDataBase?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "PinBoardApplicationRemoteData")
        app = testEnvironment!.application as? PinBoardApplicationRemoteManager
        
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
        waitForExpectations(timeout: 10, handler: nil)
        dataBase = testEnvironment!.dataBase
    
        pinList = dataBase?.pins()
    }

    override func tearDown()
    {
    }

    func test_DatabaseSHouldBeintializedFromApplication_shouldBeAccessible()
    {
        XCTAssertTrue(dataBase != nil);
    }

    func testAwsomeImageLoader_whenParsingJsonStream_10PinSHouldBePResenteInPinBoard()
    {
        let UnderTest = pinList!.count
        let Reference = 10
        XCTAssertTrue(Reference == UnderTest,"\(UnderTest) instead of \(Reference)")
    }
    
    

    
    
}
