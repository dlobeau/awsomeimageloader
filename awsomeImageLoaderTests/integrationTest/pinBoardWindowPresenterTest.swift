//
//  pinBoardWindowPresenterTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class pinBoardWindowPresenterTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:KAApplication?
    var dataBase:PinsDataBase?
    var window:PinBoardWindow?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "PinBoardApplicationRemoteData")
        app = testEnvironment!.application as? PinBoardApplicationRemoteManager
        dataBase = testEnvironment!.dataBase
        
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
                      
        waitForExpectations(timeout: 5, handler: nil)
        
        window = app?.window() as? PinBoardWindow
        window?.dataBinding()
    }

    override func tearDown() {
        
    }

    func testpinBoardWindowPresenter_collectionInWiddowSouldContain10Cell()
    {
        let Reference = 10
        let UnderTest = window!.pinBoardCollection!.section(at: 0).cells().count
        
        XCTAssert(Reference == UnderTest, "\(UnderTest) instead of \(Reference)")
    }
    
    func testpinBoardWindowPresenter_collectionInWiddowSouldBeInitalizeCorrectly()
    {
        let CheckPageConformity = window!.testdelegate?()
        CheckPageConformity!.expectSender(window!, conformTo: dataBase!)
    }

    

}
