//
//  ApplicationManagerTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 06/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class ApplicationManagerTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:KAApplication?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "PinBoardApplicationRemoteData")
        app = testEnvironment!.application as? PinBoardApplicationRemoteManager
        
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
                             
        waitForExpectations(timeout: 5, handler: nil)
              
    }

    override func tearDown()
    {
        
    }

   func testtestApplicationInitialization ()
     {
         let Reference:String = "Application main file"
         let UnderTest:String? = app?.label()
         
         XCTAssertTrue(Reference == UnderTest);
     }
     
    func testApplicationInitialization_DataBaseExtraction()
     {
         XCTAssertNotNil(app is PinBoardApplicationManagerWithLocalSource);
     }

}
