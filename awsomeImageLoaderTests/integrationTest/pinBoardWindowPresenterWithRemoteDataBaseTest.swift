//
//  pinBoardWindowPresenterWithRemoteDataBaseTest.swift
//  awsomeImageLoaderTests
//
//  Created by Didier Lobeau on 10/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest
import cleanProjectFramework

class pinBoardWindowPresenterWithRemoteDataBaseTest: XCTestCase
{
    var testEnvironment:TestEnvironment?
    var app:PinBoardApplicationRemoteManager?
    var dataBase:PinsDataBase?
    var pinList:[Pin]?
    
    

    override func setUp()
    {
        testEnvironment = TestEnvironment(WithDiContener: ContenerDI.init(), WithApplicationFileName: "PinBoardApplicationRemoteData")
        app = testEnvironment!.application as? PinBoardApplicationRemoteManager
    }

    override func tearDown()
    {
        
    }

    func testpinBoardWindowPresenterWithRemoteDataBase_collectionInWiddowSouldBeInitalizeCorrectly()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
               
        waitForExpectations(timeout: 10, handler: nil)
               
        dataBase = testEnvironment!.dataBase
        
        let window = app?.window() as? PinBoardWindow
        window?.dataBinding()
        
        let CheckPageConformity = window!.testdelegate?()
        CheckPageConformity!.expectSender(window!, conformTo: dataBase!)
    }
    
    func testpinBoardWindowPresenterWithRemoteDataBase_ImageInCell0_mustBeConformToReference()
    {
        let expectation = self.expectation(description: "didfetchingDb")
        let currentDelegate = TestApplicationStarted.init(WithExpectation: expectation)
        app?.setAsynchronousDataBindingDelegate(currentDelegate)
        app?.start()
               
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        switch result
        {
            case .completed: print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
                      
        dataBase = testEnvironment!.dataBase
        
        let window = app?.window() as? PinBoardWindow
        window?.dataBinding()
        
        let CellPresenter = window?.pinBoardCollection?.section(at: 0).cell(at: 0) as? PinCell
        
        let expectationImageLoading = self.expectation(description: "fetchingImage")
        let ImageLoaderDelegate = TestApplicationStarted.init(WithExpectation: expectationImageLoading)
        CellPresenter?.thumbnailImage?.setAsynchronousDataBindingDelegate(ImageLoaderDelegate)
        CellPresenter?.thumbnailImage?.asynchronousDataBinding!()
        var ImageUnderTest:UIImage?
        let result2 = XCTWaiter.wait(for: [expectationImageLoading], timeout:20.0, enforceOrder: true)
        switch result2
        {
            case .completed:print("Everything fulfilled")
            case .incorrectOrder: print("Unexpected order")
            case .timedOut: print("Everything did not fulfill")
            default: print("There was an issue")
        }
        
        
       ImageUnderTest = CellPresenter?.thumbnailImage?.getImage?()
        XCTAssertTrue(ImageUnderTest!.pngData()?.count == 27233)
      
    }

  

}
