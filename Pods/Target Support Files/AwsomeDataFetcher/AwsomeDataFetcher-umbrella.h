#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "AwsomeDataFetcher.h"

FOUNDATION_EXPORT double AwsomeDataFetcherVersionNumber;
FOUNDATION_EXPORT const unsigned char AwsomeDataFetcherVersionString[];

