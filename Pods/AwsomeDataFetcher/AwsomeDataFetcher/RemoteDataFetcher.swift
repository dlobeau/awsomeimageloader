//
//  RemoteImageFetcher.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 11/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import Alamofire


public protocol RemoteDataFetcher:DataFetcher
{
    var cacheMaxSize:Int{get set}
     
    func fetch(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)
    func cancel(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)->()
    
    
    var isDisableNetwork:Bool{get set}
}

public class RemoteDataFetcherAlamo:RemoteDataFetcher
{
    private var delegate: DataFetcherDelegate?
    private var cacheCurrentSize:Int = 0
    private var UrlList = [String]()
    private var cacheData:[String:Any] = [String:Any]()
    private var currentQueue:[String:DataRequest] = [String:DataRequest]()
    
    var delegateList:[DataFetcherDelegate]? = [DataFetcherDelegate]()
    public var cacheMaxSize:Int
    public  var isDisableNetwork:Bool = false;
    
    
    
    public init(WithCacheMaxSize CacheMaxSize:Int)
    {
        cacheMaxSize = CacheMaxSize
        
    }
    
    public func fetch(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)
    {
        if let val = self.getDataFromCache(WithUrl: URL)
        {
            Delegate.didFetchData(WithFetchedData: val)
        }
        else if !isDisableNetwork
        {
            let Request = Alamofire.request(URL)
            let Key = URL + Delegate.identification
            currentQueue[Key] = Request
            Request.responseData
            {
                response in self.didFetchData(WithFetchedData: response.result.value, WithURL: URL,WithDelegate: Delegate)
            }
        }
    }
    
    func getDataFromCache(WithUrl URL:String)->(Any?)
    {
       guard let val = cacheData[URL]
       else
       {
            return nil
        }
        
        if let Index = UrlList.firstIndex(of: URL)
        {//list  ordered with more recently fetch at the end
            if(Index != (UrlList.count - 1))
            {
                UrlList.remove(at: Index)
                UrlList.append(URL)
            }
        }
        return val
    }
    
    func didFetchData(WithFetchedData DataFetched:Data?, WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate)->()
    {
        let Key = URL + Delegate.identification
        currentQueue.removeValue(forKey: Key)
        if let GoodData = DataFetched
        {
            self.addToCache(WithFetchedData: GoodData, WithURL: URL)
        }
        Delegate.didFetchData(WithFetchedData: DataFetched)
    }
    
    private func addToCache(WithFetchedData DataFetched:Data, WithURL URL:String)
    {
        let NewSize = self.cacheCurrentSize+DataFetched.count
        if(NewSize >= self.cacheMaxSize)
        {//remove older item in cache
            if(!cacheData.isEmpty || UrlList.isEmpty )
            {
                self.cacheData.removeValue(forKey: UrlList.removeFirst())
            }
        }
        
        self.cacheData[URL] = DataFetched
        self.UrlList.append(URL)
        self.cacheCurrentSize = NewSize
    }
    
    private func removeFromCache(WithURL URL:String)
    {
        let ToBeRemoved = self.cacheData[URL] as? Data
        self.cacheData.removeValue(forKey: URL)
        self.cacheCurrentSize-=ToBeRemoved!.count
    }
    
    public func cancel(WithURL URL: String, WithDelegate Delegate:DataFetcherDelegate)->()
    {
        let Key = URL + Delegate.identification
        
         let DataRequestExtracted = currentQueue[Key]
        
            DataRequestExtracted!.cancel()
        
    }
        
}


