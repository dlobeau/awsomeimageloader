//
//  DataFetcher.swift
//  awsomeImageLoader
//
//  Created by Didier Lobeau on 07/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation


public protocol DataFetcherDelegate
{
     var fetchedData:Any?{get set}
     var identification:String{get}
     func didFetchData(WithFetchedData DataFectched:Any?)->()
}

public protocol DataFetcher
{
     func fetch(WithURL URL:String, WithDelegate Delegate:DataFetcherDelegate)->()
    
   
}
