The goal of this project is the contribution to the creation of a clean software development architecture framework as proposed by [Robert C Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) (Uncle bob).
[clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)


**How do i use it**:

Check the example to use the library:

* in [swift](https://gitlab.com/dlobeau/task-list-from-with-clean-architecture-in-swift)
* in [Objective C](https://gitlab.com/dlobeau/task-list-with-my-clean-architecture)




The library is available through Cocoapods

Installation of 'cleanProjectFramework' pod

*   Add pod 'cleanProjectFramework' to your podfile,
Example of podfile:
<pre><code>
# Uncomment the next line to define a global platform for your project
 platform :ios, '10.0'
source 'https://github.com/CocoaPods/Specs.git'
source 'https://gitlab.com/dlobeau/cfapodspecs.git'
target 'taskList' do
  # Comment the next line if you don't want to use dynamic frameworks
  use_frameworks!

  # Pods for taskList
	pod 'cleanProjectFramework', '~> 0.4.0', :modular_headers => true

  target 'taskListTests' do
    inherit! :search_paths
    # Pods for testing
	pod 'cleanProjectFramework', '~> 0.4.0', :modular_headers => true
  end

  target 'taskListUITests' do
    # Pods for testing
	pod 'cleanProjectFramework', '~> 0.4.0', :modular_headers => true
  end

end
</code></pre>

Don't forget to use now the .xcworkspace file instead of .xcodeproj file.



**How does it work**

The architecture of the solution is based on the MVP patterns

![image1.png](./doc/image1.png)

* **Simple composite module**: This is the library used mainly for the parsing of the XML files and for definition of the object extracted form this files.
* **ApplicationManager**: This item follow the "[coordinator pattern](http://khanlou.com/2015/01/the-coordinator/)", as the entry point of the application, setting the repository and the database for the application, also providing the window entry point. It also conforms to the "[composition root](https://freecontent.manning.com/dependency-injection-in-net-2nd-edition-understanding-the-composition-root/)", as it specified (or can receive as input), the DI containers.
* **View**: Widget en class used for the description of the application to be generated. Here, as recommended in the clean architecture, the View is "dumb", it means that code will be very light and not much is suppose to happen here.
* **Presenter**: Presenter contains the description of the view, they are plain old objects, with no reference to the library used for the widget description(no link to UIKit). They are directly generated from the XML files and are easily tested, which guaranty the compliance of the application with the expectations  
* **Domain**: Those are the object related to the business domain of the application.

Flow of action:

![image1.png](./doc/image3.png)


The architecture is based on XML interface description files used for the pages description

![image1.png](./doc/image2.png)

* group_id: the class/protocol name that will be retrieve through dependency injection 
* type_id: generated presenter object identifier (must be unique among sibling)
* identifier: identifier used for widget (optional if not widget)
* label: text display to user (optional)








