//
//  KAApplicationSerializableObjectImp.m
//  kakebo
//
//  Created by Didier Lobeau on 07/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//
@import SimpleComposite;
#import "KAApplicationSerializableObjectImp.h"

#import "KABasicFile.h"
#import "KASerializeObjectFactory.h"
#import "KASerializeObjectFactoryImp.h"
#import "KABasicFile.h"
#import "KADomain.h"
#import "KACalendarGeneric.h"
#import "KAGenericObjCInjectionDependencyDelegate.h"



//presenterTag: MainApplication
@interface KAApplicationSerializableObjectImp()


@property id<KACalendar> calendar;
@property id<KASerializeObjectFactory> factory;
@property NSDictionary<NSString *,NSString*> *widgetTag;


@end

//MainApplication injected
@implementation KAApplicationSerializableObjectImp

@synthesize calendar = _calendar;

static NSBundle *bundle;
static id<KAInjectionDependencyDelegate> InjectionDependencyDelegateDefaultItemsObject;

+(id<KAApplication>) instance
{
    static KAApplicationSerializableObjectImp *myInstance = nil;
    
    NSBundle *mainBundle = [KAApplicationSerializableObjectImp getBundle];
    NSDictionary *info = [mainBundle infoDictionary];
    
    myInstance = [info objectForKey:@"MainApplication"];
     NSAssert(myInstance != nil, @"Call createInstanceWithFileName before calling application instance"  );
    
    return myInstance;
}

+(NSBundle *) getBundle
{
    if(bundle == nil)
    {
        bundle = [NSBundle mainBundle];;
    }
    return bundle;
}

-(id) createObjectFromFamilyType:(NSString *)FamilyType
{
    return [self.factory createObjectFromFamilyType:FamilyType];
}

+(id<KAApplication>) createInstanceWithFileName:(NSString *) FileName
                                 WithDIDelegate:(id<KAInjectionDependencyDelegate>) DIDelegate
{
    NSBundle *mainBundle = [KAApplicationSerializableObjectImp getBundle];
    
    id<KAFile> File = [[KABasicFile alloc] initWithFileName:FileName WithBundle:mainBundle];
    
    NSAssert([File isFileExist], @"Main application file: %@ should doesn't exist",FileName);
    
    id<KASerializeObjectFactory> Factory = [[KASerializeObjectFactoryImp alloc] initWithWithBundle:mainBundle withDIDelegate:DIDelegate];
    
    id<KAApplication> myInstance = [Factory createWithContentsOfFile:File];
    myInstance.factory = Factory;
    
    [[mainBundle infoDictionary] setValue:myInstance forKey:@"MainApplication"];
   
    return myInstance;
    
    
}

+(id<KAInjectionDependencyDelegate>) InjectionDependencyDelegateDefaultItems
{
    if(InjectionDependencyDelegateDefaultItemsObject == nil)
    {
        NSBundle *Bundle = [KAApplicationSerializableObjectImp getBundle];
        id<KAFile> FileDomain = [[KABasicFile alloc] initWithFileName:@"domain_items" WithBundle:Bundle];
        id<KAFile> FilePresenter = [[KABasicFile alloc] initWithFileName:@"presenter_items" WithBundle:Bundle];
        id<KAFile> FileQuery = [[KABasicFile alloc] initWithFileName:@"query_items" WithBundle:Bundle];
        InjectionDependencyDelegateDefaultItemsObject = [[KAGenericObjCInjectionDependencyDelegate alloc] initWithInjectionFileList:@[FilePresenter,FileDomain,FileQuery]  ];
    }
    return InjectionDependencyDelegateDefaultItemsObject
    ;
}


-(void) start
{
    
}



-(id<KAPresenter>) window
{
    return [[self childPresenter] objectAtIndex:0];
}

-(id<KACalendar>) calendar
{
    if(_calendar == nil)
    {
        _calendar = [[KACalendarGeneric alloc] init];
    }
    return _calendar;
    
}
-(void) setCalendar:(id<KACalendar>)calendar
{
    _calendar = calendar;
}

-(id<KADomain>) datBaseContener
{
    return (id<KADomain>)[self getChildwithTypeId:@"data"];
}

-(NSDictionary *) getWidgetTagList
{
    if( self.widgetTag == nil)
    {
        self.widgetTag = [self.factory injectionFileContentWithFileName:@"UI_items"];
    }
    
     return self.widgetTag;
}

@end
