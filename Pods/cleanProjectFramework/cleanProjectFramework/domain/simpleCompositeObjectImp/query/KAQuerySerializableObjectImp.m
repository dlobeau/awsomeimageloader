//
//  KAQuerySerializableObjectImp.m
//  kakebo
//
//  Created by Didier Lobeau on 09/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KAQuerySerializableObjectImp.h"
#import "KADomain.h"
#import "KACriteria.h"
#import "KAQueryBusinessUseCase.h"
#import "KASeriazableObject.h"

@interface KAQuerySerializableObjectImp ()

@property id<KADomain> queryResult;
@property NSMutableArray<id<KACriteria>>* criterias;
@property id<KADomain> repository;
@property id<KASeriazableObject> serializableObject;

@end

@implementation KAQuerySerializableObjectImp

@synthesize criterias = _criterias;
@synthesize repository = _repository;


+(NSString *) getTypeTag
{
    return @"query";
}

-(NSArray<id<KACriteria>>*) criterias
{
    NSArray *Childs = [self childs];
    for(int i =0; i< [Childs count]; i++)
    {
        id<KASeriazableObject> currentChild = [Childs objectAtIndex:i];
        if([currentChild conformsToProtocol:@protocol(KACriteria)])
        {
            if(_criterias == nil)
            {
                _criterias = [[NSMutableArray<id<KACriteria>> alloc] init ];
            }
            [_criterias addObject:(id<KACriteria>)currentChild];
        }
    }
    return _criterias;
}

-(void) setCriterias:(NSMutableArray<id<KACriteria>> *)criterias
{
    _criterias = criterias;
}

-(id<KADomain>) repository
{
    if(_repository == nil)
    {
        _repository = (id<KADomain>)[self getChildwithTypeId:@"repository"];
    }
    return _repository;
}
-(void) setRepository:(id<KADomain>)repository
{
    _repository = repository;
}

-(void) execute
{
    id<KACriteria> Criteria= [self.criterias objectAtIndex:0];
    
    self.queryResult = [Criteria.queryBusinessUseCase executeWithRepository:self.repository WithValue:Criteria.value];
    
   
}

-(id<KADomain>) result
{
    return self.queryResult;
}

@end
