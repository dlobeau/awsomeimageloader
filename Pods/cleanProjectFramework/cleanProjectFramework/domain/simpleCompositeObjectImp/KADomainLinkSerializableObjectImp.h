//
//  KADomainLinkSerializableObjectImp.h
//  taskList
//
//  Created by Didier Lobeau on 29/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KASeriazableObjectImpLink.h"
#import "KADomainLink.h"
NS_ASSUME_NONNULL_BEGIN

@interface KADomainLinkSerializableObjectImp : KASeriazableObjectImpLink<KADomainLink>

+(id<KADomainLink>) createFromSource:(id<KADomain>) Source WithID:(NSString *) ID;


@end

NS_ASSUME_NONNULL_END
