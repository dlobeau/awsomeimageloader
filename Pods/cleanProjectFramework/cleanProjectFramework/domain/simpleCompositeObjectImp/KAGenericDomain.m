//
//  KAGenericDomain.m
//  kakebo
//
//  Created by Didier Lobeau on 02/02/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import "KAGenericDomain.h"



@interface KAGenericDomain()

@property id<KARepository> delegateRepository;


@end


@implementation KAGenericDomain

@synthesize delegateRepository = _delegateRepository;

+(NSString *) getTypeTag
{
    return @"genericDomain";
}


-(id) initWithLabel:(NSString *)label WithLabelIdentifier:(NSString *)ID WithObjectFamilyName:(NSString *)GroupId WithID:(NSString *)TypeId
{
    self = [super initWithLabel:label WithLabelIdentifier:ID WithObjectFamilyName:GroupId WithID:TypeId];
    self.isSerializable = YES;
    return self;
}

+(id) createGenericObjectWithLabel:(NSString *)Label;
{
    return [[KAGenericDomain alloc] initWithLabel:Label WithLabelIdentifier:Label WithObjectFamilyName:@"GenericDomainObject" WithID:Label];
}

-(id<KARepository>) delegateRepository
{
    return _delegateRepository;
}
-(void) setDelegateRepository:(id<KARepository>)delegateRepository
{
    _delegateRepository = delegateRepository;
}


-(BOOL) validate
{
    return YES;
}

-(void) removeItem
{
    
}

-(void) deleteItem
{
    
}
-(BOOL) deleteItemWithOptions:(NSDictionary *) options
{
    return NO;
}


-(BOOL) isDataContenerObject
{
    BOOL ReturnValue = NO;
    
    if([self.labelIdentifier isEqual:@"dataContener"])
    {
        ReturnValue = YES;
    }
    
    return ReturnValue;
}

-(NSArray<id<KADomain>> *) childs
{
    return (NSArray<id<KADomain>> *) [super childs];
}

-(id<KADomain>) requestWithID:(NSString *) RequestID
{
    return (id<KADomain>)[self getChildwithTypeId:RequestID];
}

-(void) modifyChildDomainWithId:(NSString *) Id WithValue:(id<KADomain>) NewValue
{
    
}

-(BOOL) isLink
{
    return NO;
}

@end
