//
//  KACalendar.h
//  kakebo
//
//  Created by Didier Lobeau on 21/08/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KADomain.h"

@protocol KADate;

@protocol KACalendar <KADomain>

-(id<KADate>) createCurrentDay;
-(id<KADate>) createDayWithString:(NSString *) strDay;
-(id<KADate>) createDayWithDateObject:(NSDate *) DateObject;

-(id<KADate>) NULL_DATE;

-  (NSDateFormatter *) getDateFormat;
@end
