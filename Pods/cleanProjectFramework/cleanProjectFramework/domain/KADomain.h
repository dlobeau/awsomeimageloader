//
//  KADomain.h
//  kakebo
//
//  Created by Didier Lobeau on 18/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//


@protocol KADomainLink;
@protocol KARepository;
#import "KASeriazableObject.h"

@protocol KADomain<KACompleteItemIdentification>

-(id<KARepository>) delegateRepository;
-(void) setDelegateRepository:(id<KARepository>)delegateRepository;

-(BOOL) validate;

-(void) deleteItem;

-(id<KADomain>) requestWithID:(NSString *) RequestID;
-(void) modifyChildDomainWithId:(NSString *) Id WithValue:(id<KADomain>) NewValue;

-(NSArray<id<KADomain>> *) childs;

-(BOOL) isLink;

@end
