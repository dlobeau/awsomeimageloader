//
//  KACreateUIWidgetTable.h
//  kakebo
//
//  Created by Didier Lobeau on 20/09/2018.
//  Copyright © 2018 imhuman. All rights reserved.
//

#import "KACreateUIWidgetGenericFactory.h"

@interface KACreateUIWidgetTable : KACreateUIWidgetGenericFactory

@end
