//
//  KACreateUIWidgetEditBoxFactory.m
//  taskList
//
//  Created by Didier Lobeau on 10/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KACreateUIWidgetEditBoxFactory.h"
#import "TLUITextFieldFacade.h"
#import "KAEditBox.h"

@implementation KACreateUIWidgetEditBoxFactory

-(TLUITextFieldFacade* ) createWithOwner:(id<KAView>)Owner WithWidgetType:(NSString *)WidgetType WithWidgetIdentifier:(NSString *)identifier  WithSender:(id<KAEditBox>) Sender
{
    TLUITextFieldFacade* ReturnValue = nil;
    
    ReturnValue =  (TLUITextFieldFacade *)[super createWithOwner:Owner WithWidgetType:WidgetType WithWidgetIdentifier:identifier WithSender:Sender];
    
    NSAssert([ReturnValue isKindOfClass:TLUITextFieldFacade.class], @"KACreateUIWidgetTable should create KAUITableView object");
    ReturnValue.delegate = ReturnValue;
  
    return ReturnValue;
}

-(TLUITextFieldFacade *) getChildWithID:(NSString *) ID WithWidgetParent:(id<KAView>) WidgetParent
{
    TLUITextFieldFacade * ReturnValue= (TLUITextFieldFacade*)[super getChildWithID:ID WithWidgetParent:WidgetParent];
    
    ReturnValue.delegate = ReturnValue;
    
    return ReturnValue;
    
}


@end
