//
//  TLUIICollectionViewFacade.h
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAView.h"
NS_ASSUME_NONNULL_BEGIN

@interface TLUICollectionViewFacade : UICollectionView<KAView>

-(void) registerCellsWithIDList:(NSSet<NSString *> *)IDList;

@end

NS_ASSUME_NONNULL_END
