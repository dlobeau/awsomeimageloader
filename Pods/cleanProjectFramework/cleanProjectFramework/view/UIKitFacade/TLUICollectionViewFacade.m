//
//  TLUIICollectionViewFacade.m
//  cleanProjectFramework
//
//  Created by Didier Lobeau on 09/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

#import "TLUICollectionViewFacade.h"
#import "KATable.h"
#import "TLUICollectionViewCellFacade.h"

@interface TLUICollectionViewFacade()

@property (weak) id<KADelegatePushNextView> delegatePushNextView;
@property (weak) id<KATable> interface;

@end

@implementation TLUICollectionViewFacade

-(UIView *) view
{
    return self;
}

-(void) setContent
{
    [self reloadData];
    
}

-(BOOL) validateUserChange
{
    return NO;
}

-(BOOL) addChildView:(id<KAView>) ChildView
{
    return YES;
}

- (NSString *)widgetID
{
    return self.restorationIdentifier;
}

-(void) registerCellsWithIDList:(NSSet<NSString *> *)IDList
{
    NSEnumerator<NSString *> *enumerator = [IDList objectEnumerator];
    NSString * value;
    ;
    while ((value = [enumerator nextObject]))
    {
        UINib *Nib = [UINib nibWithNibName:value bundle:nil];
        
        [self registerNib:Nib forCellWithReuseIdentifier:value];
    }
}
@end
