//
//  KAEventPageCreationWithDataObservation.h
//  taskList
//
//  Created by Didier Lobeau on 11/11/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAEventInterface.h"

NS_ASSUME_NONNULL_BEGIN

@interface KAEventPageCreationWithDataObservation : KAEventInterface

@end

NS_ASSUME_NONNULL_END
