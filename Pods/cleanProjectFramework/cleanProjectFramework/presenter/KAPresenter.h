//
//  KAUiObjectInterface.h
//  kakebo
//
//  Created by Didier Lobeau on 17/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//


@import SimpleComposite;
#import "KADataModificationOnPresenterDelegate.h"

@protocol KAEvent;
@protocol KACreateUIWidgetFactory;
@protocol KACommand;
@protocol KAPresenterConformToDomain;
@protocol KAView;
@protocol KADomain;
@protocol KACommandFactory;
@protocol KACalendar;
@protocol KAASynchronousDataBindingDelegate;

@protocol KAPresenter<KACompleteItemIdentification,KAComparable,KADataModificationOnPresenterDelegate>

//domain the presenter is initialized with
-(id<KADomain>) data;
-(void) setData:(id<KADomain>) Data;

-(NSArray<id<KAPresenter>> *) childPresenter;

//create UI associated widget
-(id<KAView> ) createViewWithOwner:(id<KAView>) Owner;

//refresh presenter data from domain
-(BOOL) dataBinding;

//property on rather the presenter must be generated as a list to mirror a data list
-(BOOL) isStaticWidget;

//update domain with presenter data
-(BOOL) validateChange;

//command to update domain with presenter data
-(NSArray<id<KACommand>> *) commands;
-(void) addCommand:(id<KACommand>) Command;
-(void) addCommands:(NSArray<id<KACommand>> *)commands;
-(void) executeCommandHierarchy;
-(void) clearCommandList;

//rather widget view should be created with parents creation or after via delegate (like cells or sections for UITableView)
-(BOOL) isInitializedOnParentCreation;

-(id<KACreateUIWidgetFactory>) viewFactoryDelegate;
-(void) setViewFactoryDelegate:(id<KACreateUIWidgetFactory>) factoryDelegate;

-(id<KACommandFactory>) commandFactoryDelegate;
-(void) setCommandFactoryDelegate:(id<KACommandFactory>) commandFactoryDelegate;

-(id<KADataModificationOnPresenterDelegate>) dataModificationDelegate;
-(void) setDataModificationDelegate:(id<KADataModificationOnPresenterDelegate>) dataModificationDelegate;

-(void) addCommandOnLabelModificationWithNewLabel:(NSString *) NewLabel WithSender:(id<KAPresenter>) Sender;
-(void) addCommandOnChildPresenterModification:(id<KAPresenter>) NewSelection WithSender:(id<KAPresenter>) Sender;
-(void) addCommandOnPresenterModificationWithPresenterWithNewValue:(id<KAPresenter>) PresenterWithNewValue WithSender:(id<KAPresenter>) Sender;
-(void) addCommandOnPresenterModificationWithNewDate:(NSDate *) NewDate WithSender:(id<KAPresenter>) Sender;
-(void) addCommandOnPresenterModificationWithBoolean:(BOOL) BooleanValue WithSender:(id<KAPresenter>) Sender;

-(void) subscribeToObservedPresenterDataModificationWithPresenter:(id<KAPresenter>) ObservedPresenter;

//delegation called when binding finished
-(id<KAASynchronousDataBindingDelegate>) asynchronousDataBindingDelegate;
-(void) setAsynchronousDataBindingDelegate:(id<KAASynchronousDataBindingDelegate>) asynchronousDataBindingDelegate;

@optional

//delegate for presenter testing
-(id<KAPresenterConformToDomain>) testdelegate;
-(void) setTestdelegate:(id<KAPresenterConformToDomain>)testdelegate;

//is the presenter can lead to an event execution
-(id<KAEvent>) event;
-(void) setEvent:(id<KAEvent>)Event;

//will tell the related UI widget if it can be enabled
-(BOOL) isEnabled;
-(void) setEnabled:(BOOL) enabled;

//will tell the related UI widget if it is visible
-(BOOL) isVisible;
-(void) setIsVisible:(BOOL) isVisible;


//will tell the related UI what text it should display
-(NSString *) getText;

//will tell the realted UI which color to be defined with
-(NSInteger) getColor;

//will tell the related UI what should be it size
-(NSInteger) getSize;

-(id<KAPresenter>) getParent;

//will tell the related UI what image it should display
-(UIImage *) getImage;

//refresh presenter data from domain
-(void) asynchronousDataBinding;



@end
