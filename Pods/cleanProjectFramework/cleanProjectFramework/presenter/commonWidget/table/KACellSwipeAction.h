//
//  KACellSwipeAction.h
//  kakebo
//
//  Created by Didier Lobeau on 28/02/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol KACellSwipeAction <KAPresenter>

@end

NS_ASSUME_NONNULL_END
