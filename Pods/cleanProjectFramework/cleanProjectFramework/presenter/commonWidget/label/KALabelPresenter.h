//
//  BVWidgetLabel.h
//  Skwipy
//
//  Created by Didier Lobeau on 17/12/2015.
//  Copyright (c) 2015 FollowTheDancer. All rights reserved.
//


#import "KAGenericPresenter.h"
#import "KALabel.h"
#import "KAPresenterConformToDomain.h"
//label injected
@interface KALabelPresenter : KAGenericPresenter <KALabel>


@end

@interface KALabelPresenterPresenterConformToDomain:NSObject< KAPresenterConformToDomain>


@end



