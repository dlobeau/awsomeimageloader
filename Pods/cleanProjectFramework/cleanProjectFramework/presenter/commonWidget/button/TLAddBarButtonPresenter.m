//
//  TLAddBarButtonPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 30/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "TLAddBarButtonPresenter.h"
#import "KAEvent.h"

@implementation TLAddBarButtonPresenter

-(id<KAPresenter>) nextAddWindow
{
    id<KAPresenter> ReturnValue =nil;
    id<KAEvent> Event = [self event];
   
    ReturnValue = [Event getDestinationWithOwner:self];
    NSAssert([ReturnValue conformsToProtocol:@protocol(KAPresenter)], @"button must be link to new page creation, check %@",self.attributeIdentity);
   
    return ReturnValue;
}

@end
