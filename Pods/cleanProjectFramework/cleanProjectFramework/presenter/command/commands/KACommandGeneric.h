//
//  KACommandGeneric.h
//  kakebo
//
//  Created by Didier Lobeau on 13/03/2017.
//  Copyright © 2017 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KACommand.h"

@interface KACommandGeneric : NSObject <KACommand>

-(id) initWithOwner:(id<KAPresenter>) Owner;


@property id<KAPresenter> owner;

@end
