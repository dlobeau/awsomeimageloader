//
//  KAGenericPresenter.m
//  taskList
//
//  Created by Didier Lobeau on 22/10/2019.
//  Copyright © 2019 imhuman. All rights reserved.
//

#import "KAGenericPresenter.h"
#import "KADomain.h"
#import "KAQuery.h"
#import "KAEvent.h"
#import "KAView.h"
#import "KABoolean.h"
#import "KACommand.h"
#import "KACreateUIWidgetGenericFactory.h"
#import "KADomainLink.h"
#import "KACommangFactoryGeneric.h"

@interface KAGenericPresenter()

@property id<KADomain> data;
    @property NSMutableArray<id<KAPresenter>> *interfaceList;
    @property id<KAPresenterConformToDomain> testdelegate;
    @property id<KACreateUIWidgetFactory> viewFactoryDelegate;

    @property id<KAEvent> event;
    @property NSMutableArray<id<KACommand>> * myCommands;
    @property id<KABoolean> isVisibleObject;
    @property id<KACommandFactory> commandFactoryDelegate;
    @property (weak) id<KADataModificationOnPresenterDelegate> dataModificationDelegate;
    @property (weak) id<KAASynchronousDataBindingDelegate> asynchronousDataBindingDelegate;

@end

@implementation KAGenericPresenter

static NSString * strEventTag = @"event";

@synthesize data = _data;
@synthesize interfaceList = _interfaceList;
@synthesize  viewFactoryDelegate = _viewFactoryDelegate;
@synthesize event = _event;
@synthesize  commandFactoryDelegate = _commandFactoryDelegate;

-(id<KADomain>) data
{
    id<KADomain> ReturnValue = _data;
    id<KAQuery> Query = [self query] ;
    if(Query != nil)
    {
        [Query execute];
        ReturnValue = [Query result];
    }
    if(ReturnValue == nil )
    {
        ReturnValue = [self getDataFromParent];
    }
    
    return ReturnValue;
}

-(void) setData:(id<KADomain,KACompleteItemIdentification>) Data
{
    _data = Data;
    
    if(self.getParent == nil)
    {
        _data = [self rootDataFromData:Data];
    }
   id<KAQuery> Query = [self query] ;
    if(Query != nil)
    {
        [Query execute];
        _data = [Query result];
    }
    [self dataBinding];
    [self.dataModificationDelegate modificationOccuredOnObservedPresenterWithSender:self];
}

-(id<KAPresenter>) getParent
{
   id<KAPresenter> ReturnValue = nil;
    
    id<KASeriazableObject> Parent = super.parent;
    
    if(Parent != nil )
    {
        if([Parent conformsToProtocol:@protocol(KAPresenter)])
        {
            ReturnValue = (KAGenericPresenter *)Parent;
        }
        else
        {
            ReturnValue = (id<KAPresenter>)Parent.parent;
            if( (ReturnValue!=nil) && ![ReturnValue conformsToProtocol:@protocol(KAPresenter)])
            {
                ReturnValue = nil;
            }
        }
    }
    return ReturnValue;
}

-(id<KADomain>) rootDataFromData:(id<KADomain,KACompleteItemIdentification>) Data
{
    id<KADomain> ReturnValue =  Data;
    if(ReturnValue != nil)
    {
        if(![self.ID isEqual:Data.ID])
        {
            ReturnValue = (id<KADomain>)[Data requestWithID:self.ID];
        }
        if(ReturnValue == nil)
        {
            ReturnValue = Data;
        }
    }
    
    return ReturnValue;
}

-(id<KAQuery>) query
{
    id<KAQuery> ReturnValue = nil;
    NSArray<id<KASeriazableObject>> *List = [super childs];
    for(int i = 0; (i< [List count]) ;i++)
    {
        id<KASeriazableObject> CurrentCHild = [List objectAtIndex:i];
        if([CurrentCHild conformsToProtocol:@protocol(KAQuery)])
        {
            ReturnValue = (id<KAQuery>)CurrentCHild;
        }
    }
     return ReturnValue;
}

-(id<KADomain>) getDataFromParent
{
    id<KADomain> ReturnValue;
    
    id<KAPresenter> Parent = (id<KAPresenter>) self.getParent;
    
    ReturnValue = (id<KADomain>)[Parent.data requestWithID:self.ID];
    if(ReturnValue == nil)
    {
        ReturnValue = Parent.data;
    }
    return ReturnValue;
}

//will tell the related UI what text it should display
-(NSString *) getText
{
    NSString * ReturnValue = self.label;
    
    if( (ReturnValue == nil) || (ReturnValue.length == 0))
    {
        id<KADomain> Data = (id<KADomain>)self.data;
        
        id<KADomain> UIWidgetData = (id<KADomain>)[Data requestWithID:self.ID];
        
        if (UIWidgetData != nil )
        {
            id<KADomain> UIWidgetDataChild = (id<KADomain>)[UIWidgetData requestWithID:self.label];
            if(UIWidgetDataChild != nil )
            {
                ReturnValue = UIWidgetDataChild.label;
            }
            else
            {
                ReturnValue = UIWidgetData.label;
            }
        }
        else
        {
            
            ReturnValue = [Data label];
        }
    }
    return ReturnValue;
}

-(id<KACommandFactory>) commandFactoryDelegate
{
    if(_commandFactoryDelegate == nil)
    {
        _commandFactoryDelegate = [[KACommangFactoryGeneric alloc] init];
    }
    return _commandFactoryDelegate;
}
-(void) setCommandFactoryDelegate:(id<KACommandFactory>) commandFactoryDelegate
{
    _commandFactoryDelegate = commandFactoryDelegate;
    
}

-(NSArray<id<KAPresenter>> *) childPresenter
{
    return self.interfaceList;
}

-(NSMutableArray<id<KAPresenter>> *) interfaceList
{
    if(_interfaceList == nil)
    {
        NSInteger Index = 0;
        id<KAIterator> Iterator = [self getIterator];
        while( [Iterator hasNext] )
        {
            id<KAPresenter,KASeriazableObject> currentChild = (id<KAPresenter,KASeriazableObject>)[Iterator next];
            
            if( [currentChild conformsToProtocol:@protocol(KAPresenter)])
            {
                if(![currentChild isStaticWidget])
                {
                    Index = [self dynamicChildInterface:currentChild AtIndex:Index];
                }
                else
                {
                    [self addWidgetPresenter:(id<KAPresenter>)currentChild];
                    Index++;
                    
                }
            }
        }
    }
    return _interfaceList;
}

-(void) addWidgetPresenter:(id<KAPresenter>) WidgetPresenter
{
    if(_interfaceList == nil)
    {
        _interfaceList = [[NSMutableArray<id<KAPresenter>> alloc] init];
    }
    if(![_interfaceList containsObject:WidgetPresenter])
    {
        [_interfaceList addObject:WidgetPresenter];
    }
}


-(void) setInterfaceList:(NSMutableArray<id<KAPresenter>> *)interfaceList
{
    _interfaceList = interfaceList;
}

-(NSInteger) dynamicChildInterface:(id<KAPresenter,KASeriazableObject>) WidgetInterface AtIndex:(NSInteger)Index
{
    NSAssert(WidgetInterface.getParent == self, @"can't retrieve dynamic child from other node parents");
    
    NSAssert(![WidgetInterface isStaticWidget], @"Check : %@, getDynamicChildWithWidgetInterface can only be performed on non static node",[WidgetInterface attributeIdentity]);
    
    NSArray<id<KADomain>> * dataList = [self getDataListWithID:WidgetInterface.ID];
    
    for(int i = 0; i< [dataList count];i++)
    {
        id<KADomain> currentData = [dataList objectAtIndex:i];
        
        id<KAPresenter,KASeriazableObject> ViewAttribute = (id<KAPresenter,KASeriazableObject>)[self widgetPresenterAtIndex:i+Index WithTemplate:WidgetInterface];
            
        ViewAttribute.ID = [currentData.ID copy];
        [ViewAttribute setParent:self];
        ViewAttribute.data = currentData;
     }
    [self trimInterfaceListAtSize:dataList];
    
    return Index+[dataList count];
}

-(NSArray<id<KADomain>> *) getDataListWithID:(NSString *) ID
{
    NSArray<id<KADomain>> * ReturnValue = nil;
    
    ReturnValue = [self getDataFromChildOfObjectWithID:ID];
        
    
    return ReturnValue;
}

-(id<KAPresenter>) widgetPresenterAtIndex:(NSUInteger) Index WithTemplate:(id<KAPresenter>) Template
{
    id<KAPresenter> ReturnValue = nil;
    if(Index < [_interfaceList count])
    {
        ReturnValue = [_interfaceList objectAtIndex:Index];
    }
    else
    {
        ReturnValue =  (id<KAPresenter>)[Template cloneObject];
        [self addWidgetPresenter:ReturnValue];
    }
    return ReturnValue;
}

-(void) trimInterfaceListAtSize:(NSArray<id<KADomain>> *) List
{
    if((List != nil) &&([List count]> 0))
    {
        NSInteger Size = [List count];
        if((Size < [self nonStaticWidgetInList:self.interfaceList]) && (Size> 0))
        {
            [self removeLastNonStaticWidgetFromList:self.interfaceList];
        }
    }
}

-(NSArray<id<KADomain>> *) getDataFromChildOfObjectWithID:(NSString *) ObjectID
{
    NSArray<id<KADomain>> * ReturnValue = nil;
    id<KADomain> Data = self.data;
    
  
   
    Data = [self.data requestWithID:ObjectID];
    
    
    if(Data != nil)
    {
        ReturnValue = Data.childs;
    }
    
    return ReturnValue;
}



-(NSInteger) nonStaticWidgetInList:(NSArray<id<KAPresenter>> *) List
{
    NSInteger ReturnValue = 0;
    for(int i =0; i< [List count];i++)
    {
        id<KAPresenter> currentValue = [List objectAtIndex:i];
        if(![currentValue isStaticWidget])
        {
            ReturnValue++;
        }
    }
    return ReturnValue;
}

-(void) removeLastNonStaticWidgetFromList:(NSMutableArray<id<KAPresenter>> *) List
{
    BOOL bFound = NO;
    for(NSInteger i =[List count]-1; (i >= 0) && !bFound;i--)
    {
        id<KAPresenter> currentValue = [List objectAtIndex:i];
        if(![currentValue isStaticWidget])
        {
            [List removeObject:currentValue];
            bFound = YES;
        }
        
    }
}

//assign UI associated widget factory
-(id<KACreateUIWidgetFactory>) viewFactoryDelegate
{
    if(_viewFactoryDelegate == nil)
    {
        _viewFactoryDelegate  = [[KACreateUIWidgetGenericFactory alloc] init];
    }
    return _viewFactoryDelegate;
}

-(void) setViewFactoryDelegate:(id<KACreateUIWidgetFactory>)viewFactoryDelegate
{
    _viewFactoryDelegate = viewFactoryDelegate;
}

//create UI associated widget
-(id<KAView>) createViewWithOwner:(id<KAView,KADataModificationDelegate>) Owner
{
    id<KAView> View = [self.viewFactoryDelegate createWithOwner:Owner WithWidgetType:self.objectFamilyName WithWidgetIdentifier:self.labelIdentifier WithSender:self];
    
    
    if(View != nil )
    {
        View.interface = self;
        
        if(Owner != nil)
        {
            View.delegatePushNextView = Owner.delegatePushNextView;
        }
        
        [self createChildsViewWithOwner:(id<KAView,KADataModificationDelegate>)View];
        
    }
    return View;
}

-(void) createChildsViewWithOwner:(id<KAView,KADataModificationDelegate>) Owner
{
    NSArray<id<KAPresenter>>* ChildList = [self childPresenter];
    
    for(int i =0; i< [ChildList count]; i++)
    {
        id<KAPresenter> currentChild = [ChildList objectAtIndex:i];
        if([currentChild isInitializedOnParentCreation])
        {
            id<KAView> NewView = [currentChild createViewWithOwner:Owner];
            
            [Owner addChildView:NewView];
            
            NewView.delegatePushNextView = Owner.delegatePushNextView;
            
        }
    }
}

//refresh presenter data from domain
-(BOOL) dataBinding
{
    BOOL bReturnValue = YES;
    
    [self resetInterfaceList];
    
    id<KADomain> Data = self.data;
    
    NSInteger count =  [self.interfaceList count];
    for(int i = 0; (i < count)  ;i++)
    {
        id<KAPresenter> CurrentInterface = [self.interfaceList objectAtIndex:i];
        
        if([CurrentInterface isStaticWidget])
        {
            id<KADomain> ChildData = (id<KADomain>)[Data requestWithID:CurrentInterface.ID];
            
            if(ChildData == nil)
            {
                ChildData = Data;
            }
             CurrentInterface.data = ChildData ;
            
        }
    }
    return bReturnValue;
}

-(void) resetInterfaceList
{
    self.interfaceList = nil;
}

//property on rather the presenter must be generated as a list to mirror a data list
-(BOOL) isStaticWidget
{
    BOOL bReturnValue = YES;
    
    id<KABoolean> AttributeValue = (id<KABoolean>)[self getChildwithTypeId:@"isStatic"];
    
    if( AttributeValue != nil)
    {
        bReturnValue = AttributeValue.value;
    }
    
    return bReturnValue;
}

//update domain with presenter data
-(BOOL) validateChange
{
    [self executeCommandHierarchy];
    [self.data validate];
    [self dataBinding];
    return NO;
}

//command to update domain with presenter data
-(NSArray<id<KACommand>> *) commands
{
    return _myCommands;
}

-(void) addCommand:(id<KACommand>) Command
{
    if( Command != nil )
    {
        if(self.myCommands == nil)
        {
            self.myCommands = [[NSMutableArray alloc] init];
        }
        [self.myCommands addObject:Command];
    }
}

-(void) addCommands:(NSArray<id<KACommand>> *)commands
{
    if(commands != nil)
    {
        self.myCommands = [[NSMutableArray alloc] initWithArray:commands];
    }
}

-(void) executeCommandHierarchy
{
    for(int i = 0; i < [self.commands count]; i++)
    {
        id<KACommand> currentCommand = [self.commands objectAtIndex:i];
        [currentCommand doCommand];
    }
    [self clearCommandList];
}

-(void) clearCommandList
{
    [self.myCommands removeAllObjects];
}


//is the presenter can lead to an event execution
-(id<KAEvent>) event
{
    if(_event == nil)
    {
        _event = (id<KAEvent>)([self getChildwithTypeId:@"event"]);
    }
    return _event;
}
-(void) setEvent:(id<KAEvent>)Event
{
    _event = Event;
    [self addChild:Event WithTypeId:strEventTag];
}




//will tell the related UI widget if it is visible
-(BOOL) isVisible
{
    return YES;
   
}
-(void) setIsVisible:(BOOL) isVisible
{
   
}

-(NSInteger) getColor
{
    
    return 0;
}

-(NSInteger) getSize
{
    return -1;
}

-(BOOL) isInitializedOnParentCreation
{
    return YES;
}

-(id<KADomain>) requestWithID:(NSString *) RequestID
{
    return nil;
}

-(NSArray<id<KAPresenter>> *) childs
{
    NSMutableArray<id<KAPresenter>> * ReturnValue;
    id<KAIterator> Iterator = [self getIterator];
    while( [Iterator hasNext] )
    {
        id<KAPresenter,KASeriazableObject> currentChild = (id<KAPresenter,KASeriazableObject>)[Iterator next];
            
        if( [currentChild conformsToProtocol:@protocol(KAPresenter)])
        {
            if(ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            [ReturnValue addObject:currentChild];
        }
    }
    
    return ReturnValue;
}

-(void) addCommandOnLabelModificationWithNewLabel:(NSString *) NewLabel WithSender:(id<KAPresenter>) Sender
{
    id<KACommand> CommandChangeLabel = [self.commandFactoryDelegate createCommandOnLabelModificationWithNewLabel:NewLabel WithSender:Sender];
    [self addCommand:CommandChangeLabel];
    
}
-(void) addCommandOnChildPresenterModification:(id<KAPresenter>) NewSelection WithSender:(id<KAPresenter>) Sender
{
    id<KACommand> Command = [self.commandFactoryDelegate createCommandOnChildPresenterModification:NewSelection WithSender:Sender];
    [self addCommand:Command];
    
}

-(void) addCommandOnPresenterModificationWithNewDate:(NSDate *) NewDate WithSender:(id<KAPresenter>) Sender
{
    id<KACommand> Command = [self.commandFactoryDelegate createCommandOnPresenterModificationWithNewDate:NewDate WithSender:Sender];
    [self addCommand:Command];
}

-(void) addCommandOnPresenterModificationWithPresenterWithNewValue:(id<KAPresenter>) PresenterWithNewValue WithSender:(id<KAPresenter>) Sender
{
    id<KACommand> Command = [self.commandFactoryDelegate createCommandOnPresenterModificationWithPresenterWithNewValue:PresenterWithNewValue WithSender:Sender];
    [self addCommand:Command];
}

-(void) addCommandOnPresenterModificationWithBoolean:(BOOL) BooleanValue WithSender:(id<KAPresenter>) Sender;
{
    id<KACommand> Command = [self.commandFactoryDelegate createCommandOnPresenterModificationWithBoolean:BooleanValue WithSender:Sender];;
    [self addCommand:Command];
}

-(void) modificationOccuredOnObservedPresenterWithSender:(id<KAPresenter>) Sender
{
    [self setData:Sender.data];
}

-(void) subscribeToObservedPresenterDataModificationWithPresenter:(id<KAPresenter>) ObservedPresenter
{
    ObservedPresenter.dataModificationDelegate = self;
}

@end
