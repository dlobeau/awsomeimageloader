//
//  KAComparable.h
//  kakebo
//
//  Created by Didier Lobeau on 05/08/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KAComparable <NSObject>

- (NSComparisonResult ) compareAttribute:(id<KAComparable> ) AttributeToBeCompared;
- (id<KAComparable>) cloneObject;

@end
