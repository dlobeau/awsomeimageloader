//
//  SimpleComposite.h
//  SimpleComposite
//
//  Created by Didier Lobeau on 14/11/2019.
//  Copyright © 2019 Didier Lobeau. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SimpleComposite.
FOUNDATION_EXPORT double SimpleCompositeVersionNumber;

//! Project version string for SimpleComposite.
FOUNDATION_EXPORT const unsigned char SimpleCompositeVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SimpleComposite/PublicHeader.h>

#import <SimpleComposite/KASerializeObjectLink.h>
#import <SimpleComposite/KASeriazableObject.h>
#import <SimpleComposite/KASeriazableObjectTable.h>
#import <SimpleComposite/KAComparable.h>
#import <SimpleComposite/KAGeneralLink.h>
#import <SimpleComposite/KACompleteItemIdentification.h>
#import <SimpleComposite/KATreeNavigable.h>
#import <SimpleComposite/KATableChildsManagementDelegate.h>


