//
//  BVDictionnaryNode.m
//  Skwipy
//
//  Created by Didier Lobeau on 04/02/2016.
//  Copyright (c) 2016 FollowTheDancer. All rights reserved.
//

#import "KADcfDictionnaryNode.h"
#import "KADcfArrayNode.h"

@implementation KADcfDictionnaryNode

-(id) initWithDictionary:(NSDictionary *) Dictionary WithNodeName:(NSString *)NodeName
{
    KADcfDictionnaryNode *ReturnValue = [super init];
    ReturnValue.dictionary = [Dictionary copy];
    ReturnValue.nodeName = [NodeName copy];
    
    return ReturnValue;
}




@end
