//
//  KADcfGenericNode.m
//  kakebo
//
//  Created by Didier Lobeau on 12/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KADcfGenericNode.h"

#import "KADcfNodeFactory.h"
#import "KADcfArrayNode.h"
#import "KASerializeObjectFactoryImp.h"
#import "KASeriazableObjectImp.h"
#import "KAFile.h"

@interface KADcfGenericNode()

@property (weak)  id<KASerializeObjectFactory> factoryDelegate;

@end

@implementation KADcfGenericNode

static NSString * ATTRIBUTE_TAG_FILE_NAME = @"AttributeTag";

-(NSDictionary *) getNodeAttributeDictionnary
{
    return self.dictionary;
}

-(NSArray<id<KADcfNode>> *) getNodeChildList
{
    NSMutableArray * ReturnValue = nil;
    
    if( self.dictionary != nil)
    {
        NSMutableDictionary *DictionnaryWithoutAttributeTag = [[NSMutableDictionary alloc] initWithDictionary:self.dictionary];
        
       id<KAFile> F = [self.factoryDelegate createFileWithFileName:ATTRIBUTE_TAG_FILE_NAME];
                
        NSArray * TagArrayList = [[NSArray alloc] initWithContentsOfFile:F.getFileCompleteName];
         
        [DictionnaryWithoutAttributeTag removeObjectsForKeys:TagArrayList];
        
        NSEnumerator *eachCategory = [DictionnaryWithoutAttributeTag objectEnumerator];
        id  CurrentChild;
        
        NSEnumerator *eachKey =[DictionnaryWithoutAttributeTag keyEnumerator];
        NSString *CurrentKey;
        
        while ((CurrentChild = [eachCategory nextObject])
               && (CurrentKey  = [eachKey nextObject]))
        {
            if( ReturnValue == nil)
            {
                ReturnValue = [[NSMutableArray alloc] init];
            }
            
            KADcfNodeFactory *Factory = [[KADcfNodeFactory alloc] init];
            id<KADcfNode> CurrentNodeChild = [Factory createParserNodeFromObject:CurrentChild WithNodeName:CurrentKey];
            CurrentNodeChild.factoryDelegate = self.factoryDelegate;
            if( CurrentNodeChild != nil)
            {
                if( [CurrentNodeChild isKindOfClass:[KADcfArrayNode class]])
                {
                    NSEnumerator *Enum =  [[CurrentNodeChild getNodeChildList] objectEnumerator];
                    id<KADcfNode> currentNOde = nil;
                    while(currentNOde = [Enum nextObject])
                    {
                        [ReturnValue addObject:currentNOde];
                    }
                }
                else
                {
                    [ReturnValue addObject:CurrentNodeChild];
                }
            }
        }
    }
    return ReturnValue;
}

-(NSString *) getNodeName;
{
    return _nodeName;
    
}

-(id<KASeriazableObject>) ParseNode:(KADcfGenericNode *) Node WithPath:(NSString *) Path
{
    id<KASeriazableObject>  ReturnValue = nil;
    
    NSDictionary *CurrentDictionnary = [Node getNodeAttributeDictionnary];
    
    ReturnValue = [self.factoryDelegate createAttributeFromDictionary:CurrentDictionnary]  ;
    
    NSArray<id<KADcfNode>>* NodeList = [Node getNodeChildList] ;
    
    NSString * NodeName = [Node getNodeName];
    if( NodeName != nil)
    {
        Path = [[Path stringByAppendingString:@"/" ] stringByAppendingString:NodeName] ;
    }
    NSEnumerator *Enum =  [NodeList objectEnumerator];
    id<KADcfNode> currentNode = nil;
    while(currentNode = [Enum nextObject])
    {
        id<KASeriazableObject> NodeAttribute  = [currentNode ParseNodeWithPath:Path];
        
        if( NodeAttribute != nil)
        {
            [ReturnValue addChild:NodeAttribute];
            
        }
    }
    return ReturnValue;
}



-(id<KASeriazableObject>) ParseNodeWithPath:(NSString *) Path
{
    id<KASeriazableObject>  ReturnValue = [self ParseNode:self WithPath:Path];
    return ReturnValue;
}



@end
