//
//  NSDictionary.m
//  kakebo
//
//  Created by Didier Lobeau on 09/12/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "NSDictionary+KASeriazableObject.h"
#import "KASeriazableObject.h"
#import "KADcfNodeFactory.h"
#import "KADcfNode.h"

@implementation NSDictionary  (KAAttribute)

+(id) dictionaryFromString:(NSString *) String
{
    NSData *plistData = [String dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    
    NSPropertyListFormat format;
    
    
    return  [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
}



-(id<KASeriazableObject>) getSeriazableObjectWithFactory:(id<KASerializeObjectFactory>) Factory
{
    id<KASeriazableObject> ReturnValue = nil;
    
    KADcfNodeFactory *FactoryNode = [[KADcfNodeFactory alloc] init];
    
    id<KADcfNode> CurrentNode = [FactoryNode createParserNodeFromObject:self WithNodeName:[[NSString alloc] initWithFormat:@"item%d",0]];
    CurrentNode.factoryDelegate = Factory;
    ReturnValue = [CurrentNode ParseNodeWithPath:@""];
         
    return ReturnValue;
}

-(NSString *) toString
{
    NSString * ReturnValue = nil;
        
    NSError *error;
    
    NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;
    
    NSData *Data = [NSPropertyListSerialization dataWithPropertyList:self format:NSPropertyListXMLFormat_v1_0 options:format error:&error];
    
    ReturnValue =  [[NSString alloc] initWithData:Data encoding:NSUTF8StringEncoding];
    
    return ReturnValue;
    
}

@end
