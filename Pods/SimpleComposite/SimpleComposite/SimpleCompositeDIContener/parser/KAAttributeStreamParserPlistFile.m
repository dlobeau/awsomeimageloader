//
//  DcfParserPlistFile.m
//  kakebo
//
//  Created by Didier Lobeau on 07/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KAAttributeStreamParserPlistFile.h"
#import "KADcfNodeFactory.h"
#import "KADcfNode.h"
#import "KASeriazableObjectTable.h"
#import "NSDictionary+KASeriazableObject.h"
#import "KAFile.h"

@interface KAAttributeStreamParserPlistFile ()



@end

@implementation KAAttributeStreamParserPlistFile


-(KAAttributeStreamParserPlistFile *) initWithFile:(id<KAFile>) FileToBeParse;
{
    KAAttributeStreamParserPlistFile *ReturnValue = [super init];
    ReturnValue.parsedFile = FileToBeParse;
  
    return ReturnValue;
}


-(id<KASeriazableObjectTable>) parseWithFactory:(id<KASerializeObjectFactory>)factory
{
    id<KASeriazableObjectTable> ReturnValue = nil;
    
    NSAssert(self.parsedFile.isFileExist , @"Error, couldn't find file: %@", self.parsedFile.fileName);
    
    NSString *FileName = self.parsedFile.getFileCompleteName;
    
    NSArray *rawRootArray = [[NSArray alloc] initWithContentsOfFile:FileName];
    
    NSAssert(rawRootArray != nil, @"Error, couldn't parse file: %@", self.parsedFile.fileName);
   
    NSDictionary *CurrentDictionary = [rawRootArray objectAtIndex:0];
    
    ReturnValue = (id<KASeriazableObjectTable>)[CurrentDictionary getSeriazableObjectWithFactory:factory];
    
    return ReturnValue;

}


@end
