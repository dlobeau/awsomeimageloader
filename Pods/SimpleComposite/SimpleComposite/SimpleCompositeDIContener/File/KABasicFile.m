//
//  DcfFile.m
//  kakebo
//
//  Created by Didier Lobeau on 08/06/2016.
//  Copyright © 2016 imhuman. All rights reserved.
//

#import "KABasicFile.h"

@interface KABasicFile()

@property NSBundle * bundle;

@end

@implementation KABasicFile


-(id) initWithFileName:(NSString *) FileName  WithBundle:(NSBundle *) Bundle
{
    KABasicFile * ReturnValue = [super init];
    
    self.fileName = FileName;
    self.bundle = Bundle;
    
    ReturnValue.completeFilePathAndName = [self copyRessourceFileWithName:FileName];
    
    return ReturnValue;
}

-(NSString * ) copyRessourceFileWithName:(NSString *) FileNameWithoutExtension
{
    NSString *FolderPath = [KABasicFile environmentRoot];
    
    return [self copyRessourceFileWithName:FileNameWithoutExtension WithFolderDestination:FolderPath];
}

+(NSString *) environmentRoot
{
    NSString * ReturnValue = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    ReturnValue = [NSString stringWithFormat:@"%@/userApplication",ReturnValue];
    
    return ReturnValue;
}

-(NSString * ) copyRessourceFileWithName:(NSString *) FileNameWithoutExtension WithFolderDestination:(NSString *) FolderName
{
    NSAssert(FileNameWithoutExtension != nil, @"Error: Request to open null file");
    
    NSString * ReturnValue = [self getCompleteFileNameWithName:FileNameWithoutExtension WithFolderPath:FolderName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:ReturnValue])
    {
        [self copyFileWithName:FileNameWithoutExtension ToDestination:FolderName];
    }
    
    return ReturnValue;
}


-(NSString *) getCompleteFileNameWithName:(NSString *) FileNameWithoutExtension WithFolderPath:(NSString *) FolderPath
{
    NSString * ReturnValue = nil;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    BOOL isDirectory;
    
    if(![fileManager fileExistsAtPath:FolderPath isDirectory:&isDirectory])
    {
        [fileManager createDirectoryAtPath:FolderPath withIntermediateDirectories:YES attributes:nil error:&error];
        
    }
    
    ReturnValue = [NSString stringWithFormat:@"%@/%@%@",FolderPath,FileNameWithoutExtension,@".plist"];
    
    return ReturnValue;
}


-(NSString *) copyFileWithName:(NSString *) FileNameWithoutExtension ToDestination:(NSString *) Destination
{
    NSString * ReturnValue = nil;
    NSError *Error;
    NSString *sourcePath = [self.bundle pathForResource:FileNameWithoutExtension ofType:@"plist"];
    if(sourcePath != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSString* NewDestination  = [NSString stringWithFormat:@"%@/%@.plist", Destination,FileNameWithoutExtension];
        
        NSAssert([fileManager fileExistsAtPath:sourcePath],@"%@ doesn't exist!",sourcePath);
        
        [fileManager copyItemAtPath:sourcePath toPath:NewDestination error:&Error];
        
        NSAssert([fileManager fileExistsAtPath:NewDestination],@"could not copy %@ to destination :%@",FileNameWithoutExtension,NewDestination);
    }
    return ReturnValue;
}


-(void) cleanFileFromUserEnvironment
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * FileName = [self completeFilePathAndName];
    NSError *Error;
    [fileManager removeItemAtPath:FileName error:&Error];
}



-(BOOL) isFileExist
{
    BOOL bReturnValue = YES;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:self.completeFilePathAndName])
    {
        bReturnValue = NO;
    }
    
    return bReturnValue;
}

-(NSString *) getFileCompleteName;
{
    return self.completeFilePathAndName;
}




@end
