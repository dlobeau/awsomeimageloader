Application for image loading in pinterest like style 

![image.png](./docs/image.png)

## Features

*  Image Database is remote Json file loaded asynchroneously.
*  Loaded image are remote and loaded asynchroneously 

## Clean architecture

This simple image loading app is a project that illustrated the [clean architecture chosen](https://gitlab.com/dlobeau/clean-project-framework-objc). 
Our method deals with the generation of the app, based on XML description language files. 
The parser for the description files and the base classes for the project are available 
through the installation of Pod in your XCODE workspace. This architecture of the app following the clean architecture rules as proposed 
by [Robert C Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) (Uncle bob).
 
This project illustrate how the clean archiecture framework handle multithreading task 

## Dependency

for the clean architecture:
https://gitlab.com/dlobeau/clean-project-framework-objc

for the data fetching:
https://gitlab.com/dlobeau/awsomefilefetcher

[SwiftInject](https://github.com/Swinject/Swinject) for as Dependency Injection container




